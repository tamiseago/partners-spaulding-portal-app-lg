import $ from 'jquery';
import Backbone from 'backbone';
import Pelican from 'pelican';

const Router = Pelican.Router.extend({

    // automatic screen routes
    appRoutes: {
        '': '',
        'discharged': 'DischargedScreen',
        'discharged/tv': 'TvScreen',
        'intro': 'IntroScreen',
        'intro2': 'Intro2Screen',

        // movies & tv
        'videoplayer/:id': 'VideoPlayerScreen',
        'tv': 'TVScreen',
        'music': 'MusicScreen',
        'home': 'PrimaryScreen',
        'series/:id': 'EpisodeSynopsisScreen',
        'movie/:id': 'MovieSynopsisScreen',
        'movieplayer/:id': 'MoviePlayerScreen',
        'home/entertainment/free-movies': 'MovieScreen',
        'home/entertainment/free-movies-spanish': 'MovieScreen',
        'home/entertainment/tv-comedies/tv-comedies/:series': 'VideoSeriesEpisodesScreen',
        'home/entertainment/tv-comedies': 'VideoSeriesScreen',
        'home/entertainment/watch-tv': 'TVGuideScreen',
        'home/entertainment/music-radio' : 'MusicGuideScreen',

        // health/education
        'education/:razuna': 'EDSynopsisScreen',
        'educationvideo/:razuna': 'EducationVideoPlayerScreen',
        'edlibrary/:razuna': 'EducationVideoPlayerScreen',
        'home/my-health/my-health-videos': 'MyProgramScreen',
        'home/my-health/health-library': 'EdLibraryScreen',
        'home/my-health/recovery-stories': 'RecoveryStoriesScreen',

        // patient meals
        'internet': 'WebPageScreen',

        // my care
        'home/my-care/welcome-video' : 'EducationVideoPlayerScreen',

        // my-comfort
        'home/relax/music-radio' : 'MusicGuideScreen',
        'home/my-comfort/relaxation-channel': 'PlayListPlayerScreen',
        'home/my-comfort/sleep-enhancement': 'SleepEnhancementScreen',
        'relaxation/:razuna': 'RelaxationPlayerScreen',

        // everything else is gonna use this
        'home/:group/:id': 'SecondaryScreen'
    },

    // To avoid writing too many switch/case, we let webpack pack everything in ./screens/ and require them dynamically.
    // This function will be called during routing check.  Implement this function so it reflects to your screen folder
    // structure.
    getScreen: function (name) {
        try {
            var ScreenClass = require('./screens/' + name.toLowerCase() + '.js').default;
        }
        catch (e) {
            console.log('Screen not found. ' + e.toString());
            return undefined;
        }
        return ScreenClass;
    },

    // this function will be called right after router started
    onStart: function () {
        var startPath = this.getFullPath();
        if (App.data && App.data.patient && App.data.patient.mrn) { // admitted
            if (!startPath || startPath.length < 2) {
                if (App.data.patient.patientPreferences) { // check if accepted term
                    var terms = $.grep(App.data.patient.patientPreferences, function (e) {
                        return e.key == 'term';
                    });
                    if (terms.length > 0) {
                        window.location.href = '#home';
                    }
                    else {
                        window.location.href = '#intro';
                    }
                }
                else {
                    window.location.href = '#intro';
                }
            }
            else if (startPath != 'home' && startPath != 'intro') {
                // deep linking for developer only.  always display home page first then this page
                App.layout.closeAllScreens();
                window.location.href = '#home';
                setTimeout(function () {
                    window.location.href = '#' + startPath;
                }, 50);
            }
        }
        else { // discharged
            window.location.href = '#discharged';
        }
        console.log('landing route:' + this.getFullPath());
    }

});

export default Router;
