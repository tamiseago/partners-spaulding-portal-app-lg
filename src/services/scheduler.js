var KeyCodes = require('pelican-device').KeyCodes;
import Radio from 'backbone.radio';
import '../helpers/dotimeout';

class Scheduler {

    constructor() {
        this.firstAdtPoll = true;
        this.cheats = {};
        this.cheats.reload = [KeyCodes.Menu, KeyCodes.Num2, KeyCodes.Num5, KeyCodes.Num8, KeyCodes.Num0];
    }

    start(options) {
        var self = this;

        var now = new Date();
        App.data.bootTime = now.getTime();

        // ADT
        var adtPollInterval = App.config.adtPollInterval || 60000;
        $.doTimeout('ADT poll', adtPollInterval, function () {
            self.getPatient();
            return true;
        });

        // Update device
        $.doTimeout('update ip and firmware', 10000, self.updateDevice);

        // Nightly reload, checks every 59 minutes
        $.doTimeout('auto reload', 3540000, self.autoReload);

        // cheat codes
        var channel = Radio.channel('device');
        channel.on('key', function (e, key) {
            self.checkCheatCodes(key);
        }, this);
    }

    getDevice() {
        var self = this;
        var deferred = $.Deferred();

        App.upserver.api('/device').done(function (data) {

            App.data.device = data;
            upserver.trigger(upserver.events.device.locationUpdated);

        }).always(function () {
            deferred.resolve();
        });

        return deferred.promise();
    }

    getPatient() {
        var self = this;
        var deferred = $.Deferred();

        upserver.api('/me').done(function (data) {
            console.log('room status: admitted');
            var oldPatient = App.data.patient;
            App.data.patient = data;
            if (self.firstAdtPoll) {
                console.log('ignore status when starting up...');
                self.firstAdtPoll = false;
                upserver.trigger(upserver.events.patient.profileChanged);
                return;
            }

            var fragment = Backbone.history.getFragment();
            if (fragment.startsWith('discharged')) {
                // if currently on discharged page, reload
                console.log('transition from discharged to admitted, reloading...');
                upserver.trigger(upserver.events.device.patientAssociated);
                upserver.trigger(upserver.events.patient.visitChanged);
            }
            else if (oldPatient.mrn != data.mrn) {
                // different patient
                console.log('a different patient admitted, reloading...');
                upserver.trigger(upserver.events.device.patientAssociated);
                upserver.trigger(upserver.events.patient.visitChanged);
            }
        }).fail(function () {
            console.log('room status: discharged');
            App.data.patient = null;
            if (self.firstAdtPoll) {
                console.log('ignore status when starting up...');
                self.firstAdtPoll = false;
                return;
            }

            //no patient
            var fragment = Backbone.history.getFragment();
            if (!fragment.startsWith('discharged') && !fragment.startsWith('ehospital')) {
                // previously has patient, now no patient, fire events up
                console.log('transition from admitted to discharged, reloading...');
                upserver.trigger(upserver.events.device.patientDissociated);
                upserver.trigger(upserver.events.patient.visitChanged);
            }
        }).always(function () {
            self.firstAdtPoll = false;
            deferred.resolve();
        });

        return deferred.promise();
    }

    updateDevice() {
        upserver.api('/device', 'PUT', {
            firmware: App.data.firmware,
            ipAddress: App.data.ip
        });
    }

    autoReload() {
        var now = new Date();
        var dailyReloadHour = App.config.dailyReloadHour || 3;
        if (now.getHours() == dailyReloadHour) {
            if (PelicanDevice.reboot)
                PelicanDevice.reboot();
        }
        return true;
    }

    checkCheatCodes(key) {
        if (this.isCheat(key, this.cheats.reload)) {
            if (PelicanDevice.reload)
                PelicanDevice.reload();
        }
    }

    isCheat(key, cheatKeys) {
        var idx = cheatKeys.match || 0;
        var result = false;

        if (!cheatKeys || !cheatKeys.length || !key)
            return false;

        if (idx < 0 || idx >= cheatKeys.length) {
            idx = 0;
            result = false;
        }
        else if (cheatKeys[idx] === key) {
            idx++;   // hit, increase match index

            if (idx >= cheatKeys.length) {    // bingo, all hit
                console.log('^-^ ^-^ ^-^ ^-^ ^-^ ^-^ ^-^ ^-^ ^-^ ^-^ ^-^ ^-^ ^-^ ^-^ ');
                console.log('^-^ cheat code detected - "' + cheatKeys.toString() + '" ^-^');
                console.log('^-^ ^-^ ^-^ ^-^ ^-^ ^-^ ^-^ ^-^ ^-^ ^-^ ^-^ ^-^ ^-^ ^-^ ');
                idx = 0;
                result = true;
            }
            else {
                result = false;
            }
        }
        else {
            idx = 0; // missed, reset index
            result = false;
        }

        cheatKeys.match = idx;
        return result;
    }
}

export default Scheduler;