import Pelican from 'pelican';
import ContentButtonGroupCheckList from '../contentbuttongroupchecklist/controller';

const ContentMenuCheckList = Pelican.ContentMenu.extend({
    widgets: {
        folders: {
            widgetClass: Pelican.ContentButtonGroup,
            selector: '.major',
            options: {backButton: true, nonActive: true, contentClass: 'folder'}
        },
        assets: {
            widgetClass: ContentButtonGroupCheckList,
            selector: '.minor',
            options: {backButton: false, nonActive: false, contentClass: 'asset'}
        }
    },
});

export default ContentMenuCheckList;