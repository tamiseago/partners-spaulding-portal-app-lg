import Pelican from 'pelican';
import SetCenter from '../../helpers/setcenter';
import Intl from '../../helpers/intl';
require('../../css/dialog.css');
require('./style.css');
var template = require('./template.hbs');

/**
 * Dialog is a widget you can throw into a screen that handles pop-up style modal dialog
 */
const FeedbackDialog = Pelican.Dialog.extend({
    className: 'dialog welcome-feedback',
    template: template,

    events: {
        'click #close': 'onClose'
    },

    onKey: function(e, key){
        var handled = false;

        switch(key) {
            case "UP":
                this.moveFocus('prev', $('.welcome-feedback #buttons > .button-group.widget'));
                handled = true;
                break;
            case "DOWN":
                this.moveFocus('next', $('.welcome-feedback #buttons > .button-group.widget'));
                handled = true;
                break;
            case "ENTER":
            case "OK":
            case "SELECT":
                this.click('.welcome-feedback .active');
                handled = true;
                break;
            default:
                break;
        }

        return handled;
    },

    onInit: function(){
    },

    onClose: function(){
        this.hide();
    },

    onShow: function(){
        SetCenter.setCenter(this.$('.container'));
    },

    onRender: function () {

        var buttons = {
            buttons: [
                {id: 'close', text: Intl.translate('Close'), className: 'button active'}

            ]
        };

        App.data.epicResults.understanding = 'NR';
        this.getWidget('buttons').model.set(buttons);
        this.focus('#close');

    },

    hide: function(){
        FeedbackDialog.__super__.hide.apply(this, arguments);

        this.onHide();
    },

    onHide: function(){},
});

export default FeedbackDialog;