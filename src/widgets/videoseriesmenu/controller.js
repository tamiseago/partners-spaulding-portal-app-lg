import Pelican from 'pelican';
import  SetCenter from '../../helpers/setcenter';

require('./style.css');
var template = require('./template.hbs');

const VideoSeriesMenu = Pelican.TabMenu.extend({
    template: template,
    className: 'videoSeriesMenu',

    events: {
        "click .menu-tab-button": "selectSeries"
    },

    onInit: function(){
        VideoSeriesMenu.__super__.onInit.apply(this, arguments);
    },

    selectSeries: function(e){
        var target = $(e.currentTarget);
        var uri = 'series/' + target.attr('data-current-slug');
        var handled = true;

        console.log('VideoSeriesMenu.selectSeries.uri ' + uri);
        console.log('VideoSeriesMenu.selectSeries.id ' + target.attr('id'));

        App.router.go(uri);
    }
});

export default VideoSeriesMenu;

