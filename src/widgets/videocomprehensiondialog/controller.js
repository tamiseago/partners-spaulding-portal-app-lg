import Pelican from 'pelican';
import SetCenter from '../../helpers/setcenter';
import Intl from '../../helpers/intl';

require('./style.css');
var template = require('./template.hbs');

/**
 * Dialog is a widget you can throw into a screen that handles pop-up style modal dialog
 */
const VideoComprehensionDialog = Pelican.Dialog.extend({
    className: 'dialog welcome-comprehension',
    template: template,

    events: {
        'click #no': 'onNo',
        'click #yes': 'onYes',
        'click #escape': 'onEscape'
    },

    onKey: function(e, key){
        var handled = false;

        switch(key) {
            case "UP":
                this.moveFocus('prev', $('.welcome-comprehension #buttons > .button-group.widget'));
                handled = true;
                break;
            case "DOWN":
                this.moveFocus('next', $('.welcome-comprehension #buttons > .button-group.widget'));
                handled = true;
                break;
            case "ENTER":
            case "OK":
            case "SELECT":
                this.click('.welcome-comprehension .active');
                handled = true;
                break;
            case "MENU":
                /**
                 * i could not call this.onEscape() direclty.i had to 
                 * executed a dom event in order to access it. not sure 
                 * why that is the case. there must be a something
                 * getting in the way.
                 */
                this.click('.welcome-comprehension #escape');
                handled = true;
                break;
            default:
                break;
        }

        return handled;
    },

    onShow: function(){
        SetCenter.setCenter(this.$('.container'));
        this.getWidget('buttons').focus('#yes');
        console.log('VideoComprehensionDialog.onShow');
    },

    onYes: function () {
        App.data.epicResults.understanding = 'VU';
        console.log('VideoComprehensionDialog.onYes');

        this.hide();
    },

    onNo: function () {
        App.data.epicResults.understanding = 'NR';
        console.log('VideoComprehensionDialog.onNo');

        this.hide();
    },

    onEscape: function () {
        App.data.epicResults.understanding = 'NL';
        console.log('VideoComprehensionDialog.onEscape');

        this.hide();
    },

    onRender: function () {

        var buttons = {
            buttons: [
                {id: 'yes', text: Intl.translate('Yes'), className: 'button active'},
                {id: 'no', text: Intl.translate('No'), className: 'button'},
                {id: 'escape', text: Intl.translate('Escape'), className: 'button hidden'}

            ]
        };
        console.log('VideoComprehensionDialog.onRender');

        // App.data.epicResults.understanding = 'NR';
        this.getWidget('buttons').model.set(buttons);

    },

    onInit: function(){
    },

    hide: function(){
        VideoComprehensionDialog.__super__.hide.apply(this, arguments);
        console.log('VideoComprehensionDialog.hide');

        this.onHide();
    },

    onHide: function(){},
});

export default VideoComprehensionDialog;