import Pelican from 'pelican';
import SetCenter from '../../helpers/setcenter';
import Intl from '../../helpers/intl';
require('../../css/dialog.css');
require('./style.css');
var template = require('./template.hbs');

/**
 * Dialog is a widget you can throw into a screen that handles pop-up style modal dialog
 */
const MedicationPinErrorDialog = Pelican.Dialog.extend({
    className: 'dialog pinerror-dialog',
    template: template,

    events: {
        'click #close': 'close'
    },

    onKey: function(e, key){
        var handled = false;

        switch(key) {
            case "ENTER":
            case "OK":
            case "SELECT":
                this.close(e);
                handled = true;
                break;
            default:
                break;
        }

        return handled;
    },

    onInit: function(){
    },

    close: function(e){
        this.hide();
        this.onClose(e);
    },

    onShow: function(){
        this.model.set("hasFocus", true);
        SetCenter.setCenter(this.$('.container'));
    },

    onRender: function () {

        var buttons = {
            buttons: [
                {id: 'close', text: Intl.translate('Close'), className: 'button active'}

            ]
        };

        App.data.epicResults.understanding = 'NR';
        this.getWidget('buttons').model.set(buttons);
        this.focus('#close');

    },

    hide: function(){
        MedicationPinErrorDialog.__super__.hide.apply(this, arguments);

        this.model.set("hasFocus", false);
        this.onHide();
    },

    onHide: function(){},
    onClose: function(){}
});

export default MedicationPinErrorDialog;