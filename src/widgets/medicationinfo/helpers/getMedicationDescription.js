var handlebarsRuntime = require("handlebars/runtime");

export default function(value) {
    var description =  JSON.parse(value.replace(/\"\"/g, '"')).description;
    var result = new handlebarsRuntime.default.SafeString(description);
    return (result == "NULL") ? "No Description" : result;
}