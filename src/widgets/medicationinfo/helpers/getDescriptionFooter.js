var handlebarsRuntime = require("handlebars/runtime");

export default function(value) {
    var descriptionFooter =  JSON.parse(value.replace(/\"\"/g, '"')).descriptionFooter;
    var result = new handlebarsRuntime.default.SafeString(descriptionFooter);
    return (result == "NULL") ? "" : result;
}