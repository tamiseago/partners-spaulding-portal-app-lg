var handlebarsRuntime = require("handlebars/runtime");

export default function(value) {
    var name =  JSON.parse(value.replace(/\"\"/g, '"')).name;
    return new handlebarsRuntime.default.SafeString(name);
}