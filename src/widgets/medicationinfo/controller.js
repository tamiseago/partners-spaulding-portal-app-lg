import Pelican from 'pelican';
import  SetCenter from '../../helpers/setcenter';

require('./style.css');
var template = require('./template.hbs');

const MedicationInfo = Pelican.Widget.extend({
    template: template,

    events: {
        "click .btn-med-desc-open": "openDesc",
        "click .btn-med-desc-close": "closeDesc"
    },

    getRxInfo: function (type) {
        var self = this;
        var defer = jQuery.Deferred();
        
        upserver.api('/me/clinicalrecords?type=' + type + '&sort=value', 'GET')
            .done(function (data) {
                console.log("MedicationInfo.getRxInfo.data");
                console.log(data);
                self.model.set('medications', data);
                defer.resolve(self.model.get('medications'));
            })
            .fail(function (f) {
                defer.reject("MedicationInfo.getRxInfo.error", f.errorMessage);
            });

        return defer.promise();
    },

    onKey: function(e, key){
        console.log("MedicationInfo.onKey.key " + key);
        var handled = false;

        if (this.model.get('hasFocus') == false)
            return handled;
        
        var next = 'next';
        var prev = 'prev';
        var container = this.$('#medlist');
        var selector = "li";
        var step = 1;
        var focusClass = 'active';

        switch(key){
            case 'DOWN':
                console.log("MedicationInfo.onKey.key Going DOWN");
                this.moveFocus(next, container, selector, step, focusClass);
                handled = true;
                break;
            case 'UP':
                console.log("MedicationInfo.onKey.key Going UP");
                this.moveFocus(prev, container, selector, step, focusClass);
                handled = true;
                break;
            case 'ENTER':
            case 'SELECT':
                this.onSelect();
                break;
            case 'LEFT':
            case 'ESCAPE':
                var desc = $("#medlist .open .med-desc");
                var close = $('.btn-med-desc-close', desc);
                if(desc.length > 0){
                    this.click(close)
                } else {
                    this.model.set('hasFocus', false);
                }
            default:
                break;
        }

        return handled;
    },

    /* moveFocus: function (direction, container, selector, step, focusClass, noFocus) {
        MedicationInfo.__super__.apply(this, arguments);
        var obj = this.$el;
        var childPos = obj.offset();
        var parentPos = obj.parent().offset();
        var childOffset = {
            top: childPos.top - parentPos.top,
            left: childPos.left - parentPos.left
        }
    }, */

    onSelect: function(){
        var active = $('#medlist li.active a');
        var desc = $("#medlist .open .med-desc");

        var close = $('.btn-med-desc-close', desc);

        if(desc.length > 0){
            this.click(close);
            console.log('MedicationInfo.controller.onSelect CLOSE');
        } else {
            this.click(active);
            console.log('MedicationInfo.controller.onSelect OPEN');
        }
    },

    onModelChange: function(e){
        console.log('MedicationInfo.onModelChange');
    },

    openDesc: function(e){
        var t = $(e.target);
        var desc = "#" + t.attr('id').replace("-name", "-desc") + " ";
        t.parent().parent().addClass('open');
        this.$('.med-name').hide();

        var dtl = this.$('#medication-detail');
        dtl.html(this.$(desc).html());
        dtl.show();

        console.log('MedicationInfo.openDesc');
    },
    closeDesc: function(e){
        var t = $(e.target);
        this.$('.med-name').show();
        t.parent().parent().removeClass('open');

        var dtl = this.$('#medication-detail');
        dtl.html('');
        dtl.hide();

        var container = this.$('#medlist');
        var active = this.$("#medlist > li.active");
        this.scrollTo(active, container);

        console.log('MedicationInfo.closeDesc');
    },

    getMedName: function(val){
        console.log('MedicationInfo.getMedName', val);
        return val;
    },

    onInit: function(){
        this.model.set('hasFocus', false);
    },

    onRender: function(){
        
    },

    onFocus: function(){
        console.log("MedicationInfo.onFocus");
    },

    onBlur: function(){
        this.blur(this.$el);
        console.log("MedicationInfo.onBlur");
    },
});

export default MedicationInfo;

