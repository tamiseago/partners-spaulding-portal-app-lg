var handlebarsRuntime = require("handlebars/runtime");
var i18n = require('i18next');

export default function(value) {
    var result = i18n.t(value);
    return new handlebarsRuntime.default.SafeString(result);
}
