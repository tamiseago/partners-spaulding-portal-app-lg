import Pelican from 'pelican'

require('./style.css');
var template = require('./template.hbs');

const CareTeamMemberList = Pelican.Widget.extend({
    maxPerSlide:7,
    template: template,
    loopSlides: true,

    organizeMemberList: function(rawmems){
        var doctors = [];
        var others = [];
        var data = rawmems;
        var cnt = 0;

        for (var i = 0; i < data.length; i++) {
            var value = data[i].value;
            var code = data[i].code;
            var desc = data[i].codeDescription;
            var endDate = new Date(data[i].endDateTime);
            var endYear = endDate.getYear()
            console.log(endYear)
            var minValue = '-62135575764000'
            var endDateValue = endDate.valueOf()
            var id = data[i].id;
            var now = new Date();

            /* if(cnt>=9) {
                var doctors = this.$('.page2 #doctors ul');
                var others = this.$('.page2 #others ul');
            } else if(cnt>=18) {
                var doctors = this.$('.page3 #doctors ul');
                var others = this.$('.page3 #others ul');
            } */

            if (endDateValue <= minValue || (endDate >= now)) {
                var careteam = value;
                if (desc) {
                    careteam = value + ', ' + desc;
                } else {
                    careteam = value;
                }

                var li = {data: careteam};
                if (code <=99) {
                    li.css = 'doctor';
                    doctors.push(li);
                } else {
                    var css = (others.length) ? 'other' : 'other first';
                    li.css = css;
                    others.push(li);
                }
            }
        }
        
        var final = jQuery.merge(doctors, others);
        console.log('CareTeamMemberList.organizeMemberList ORGANIZED CARE TEAM LIST: ', final);

        return final;
    },

    chunkMemberList: function(rawmems){
        var rawmems = this.organizeMemberList(rawmems);
        var mps = this.maxPerSlide; // max members per slide
        var total = rawmems.length; // total number of members
        var listCount = 0; // total numebr of member lists
        var memCount = 0; // number of memebrs in a list. this will reset for each list
        var mems = []; // the chunked list of memebers
        mems[listCount] = [];
        
        for(var i = 0; i < total; i++) {

            var ctm = rawmems[i];
            mems[listCount][memCount] = ctm;

            if(memCount >= mps) {
                memCount = 0;
                listCount++;
                mems[listCount] = [];
            } else {
                memCount++;
            }
        }
        this.listCount = listCount;
        this.model.set('members', mems);
        console.log('CareTeamMemberList.chunkMemberList members: ', mems);
    },

    onKey: function(e, key){
        var handled = false;
        var hasFocus = this.isFocused();
        if(!hasFocus) { return false; }

        console.log('CareTeamMemberList.onKey We have focus and are applying key ' + key);

        var memberSlides = this.$('#careteam > li');
        var listCount = memberSlides.length;
        var activeIndex = this.getActiveElementIndex();
        var nextIndex = 0;

        switch(key) {
            case 'LEFT': //previous
                nextIndex = this.previous(listCount, activeIndex);
                handled = true;
                break;
            case 'RIGHT': // next
                nextIndex = this.next(listCount, activeIndex);
                handled = true;
                break;
            default:
                break;
        }
        memberSlides.removeClass('active');
        console.log('CareTeamMemberList.onKey memberSlides length: ' + memberSlides.length);

        if(handled) {
            $(memberSlides[nextIndex]).addClass('active');
            console.log('CareTeamMemberList.onKey nextIndex: ' + nextIndex);
        }

        return handled;
    },

    next: function(total, index){
        var ni = index + 1;
        if(this.loopSlides) {
            ni = (ni >= total) ? 0 : ni;
        } else {
            ni = (ni >= total) ? index : ni;
        }
        
        console.log('CareTeamMemberList.next ni = ' + ni);
        return ni;
    },

    previous: function(total, index){
        var pi = index - 1;
        var last = total - 1;

        if(this.loopSlides) {
            pi = (pi < 0) ? last : pi;
        } else {
            pi = (pi < 0) ? index : pi;
        }
        
        console.log('CareTeamMemberList.previous pi = ' + pi);

        return pi;
    },

    focus: function(){
        this.$el.addClass('focused');
        this.$el.show();
        /**
         * had to show the arrows this way. could not tap into
         * them wihtout a delay. the tabmenu widget's showTab method is called
         * after this even, therefor hiding the arrows because there are
         * no subpages.
         */
        if(this.listCount >= 1) {
            $.doTimeout(100, function () {
                $('.arrow-button').show();
            });
            $('#menu2-tab .sub-footer').html(i18n.t("Use Right/Left to scroll Care Team"));
        }
        console.log('CareTeamMemberList.onFocus: we have focused the Care Team List');
    },

    isFocused: function(){
        return this.$el.hasClass('focused');
    },

    blur: function(){
        this.$el.removeClass('focused');
        this.$el.hide();
        $('.arrow-button').hide();
        console.log('CareTeamMemberList.onBlur: we have blured the Care Team List');
    },

    getActiveElementIndex: function(){
        var index = 0;
        this.$('li.memberlist').each(function(i){
            if($(this).hasClass('active')) {
                index = i
            }
        });

        console.log('CareTeamMemberList.getActiveElementIndex index: ' + index);
        return index;
    }
});

export default CareTeamMemberList;
