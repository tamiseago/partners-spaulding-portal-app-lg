import Pelican from 'pelican';

require('./style.css');
var template = require('./template.hbs');

/**
 * Dialog is a widget you can throw into a screen that handles pop-up style modal dialog
 */
const PinInput = Pelican.Widget.extend({
    className: 'pin-input',
    template: template,

    ui: {
        pin1: "#pin1",
        pin2: "#pin2",
        pin3: "#pin3",
        pin4: "#pin4"
    },

    events: {
        "focus input": "onFocus",
        "blur .pin-number": "onBlur",
        "change @ui.pin1": "updatePin",
        "change @ui.pin2": "updatePin",
        "change @ui.pin3": "updatePin",
        "change @ui.pin4": "updatePin",
    },

    widgets: {
    },

    activeWidget: {},
    activePin: "pin1",

    getNextPinId: function(direction){
        var index = parseInt(this.activePin.substring(3,4));
        var next  = index + 1;
        var back = index - 1;

        var npin = (next > 4) ? 1 : next ;
        var bpin = (back < 1) ? 4 : back;
        var pnext = "";

        if (direction == 'next' || direction == 'right') {
            pnext = 'pin' + npin
        } else if (direction == 'back' || direction == 'left') {
            pnext = 'pin' + bpin
        }

        console.log('PinInput.getNextPinId next: ' + next);

        return pnext;
    },

    updatePin: function(e){
        var pin = $(e.target).attr('id');
    },

    onKey: function(e, key){
        e.preventDefault();
        var nums = [1,2,3,4,5,6,7,8,9,0];
        var directions = ["LEFT", "RIGHT"];
        var next = "#";
        var handled = false;
        
        if($.inArray(parseInt(key), nums) > -1) {
            this.model.set(this.activePin, key);
            var pid = this.getNextPinId('next');
            next += pid;
            console.log('PinInput.onKey model.value:' + 
                '\nactivePin: ' + this.activePin + 
                '\nmodel.pin (active): ' + this.model.get(this.activePin) +
                '\nnext: ' + next);

            var pin = this.ui.pin1.val() +
            this.ui.pin2.val() +
            this.ui.pin3.val() +
            this.ui.pin4.val();

            this.model.set('pin', pin);
            console.log('PinInput.onKey PIN NUMBER:' + 
            + '\nnext: ' + next +
            '\nmodel.pin' + this.model.get('pin'));
            this.$(next).focus();
            // this.moveFocus('next', this.$el, this.$(next));
            handled = true;

        }

        return handled;
    },

    onFocus: function(e){
        var target = $(e.target);
        this.activePin = target.attr('id');

        this.$('input').removeClass('active');
        target.addClass('active');

        console.log('PinInput.onFocus ID:  ' + this.activePin);

    },

    onBlur: function(e){
        var target = $(e.target);
        this.$('input').removeClass('active');
        console.log('PinInput.onBlur');
    },

    onInit: function(){
        this.model.set({
            pin1: "",
            pin2: "",
            pin3: "",
            pin4: ""
        });
    },

    onClose: function(){
    },

    onShow: function(){
        this.ui.pin1.focus();
    },

    onRender: function () {
        this.ui.pin1.val(this.model.get('pin1'));
        this.ui.pin2.val(this.model.get('pin2'));
        this.ui.pin3.val(this.model.get('pin3'));
        this.ui.pin4.val(this.model.get('pin4'));
    },

    resetPin: function(){
        this.model.set({
            pin1: "",
            pin2: "",
            pin3: "",
            pin4: "",
            pin: ""
        });

        this.ui.pin1.addClass('active');
    }
});

export default PinInput;