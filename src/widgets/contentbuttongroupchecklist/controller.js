import Pelican from 'pelican';

const ContentButtonGroupCheckList = Pelican.ContentButtonGroup.extend({
    buttonData: [],

    getBookmark: function(bid){
        // get bookmark data from upserver
        var defer = $.Deferred();
        upserver.api('/me/bookmarks/' + bid)
            .done(function(bkmrk){
                console.log('we got the bookmark');
                defer.resolve(bkmrk);
            })
            .fail(function(){
                defer.reject('unable to retrieve a bookmark');
                console.log('unable to find a bookmark');
            });
        return defer;
    },

    getVideoByRazunaId: function(rid){
        // find the button matching the razuna id
        for(var i = 0; i < this.buttonData.length; i++){
            var button = this.buttonData[i];
            if(rid == button.contentId) {
                console.log('found button', button);
                return button;
            }
        }

        // at this point we've found nothing.
        return false;
    },

    isPrescribed: function(oid){
        if(oid) {
            return true;
        }

        return false;
    },
    
    isCompleted: function(status){
        if(status == 'Completed') {
            return true;
        }

        return false;
    },

    hasBookmark: function(vid){
        var defer = $.Deferred();

        if(vid.bookmarkIds && vid.bookmarkIds.length > 0) {
            var index = vid.bookmarkIds.length - 1;
            var bid = vid.bookmarkIds[index];
            this.getBookmark(bid)
                .done(function(bkmrk){
                    console.log('edu bookmark', bkmrk);
                    defer.resolve(bkmrk);
                });
        } else {
            return false;
        }

        return defer;
    },

    onRender: function () {
        // make sure we run all other on render items
        ContentButtonGroupCheckList.__super__.onRender.apply(this);
        this.buttonData = this.model.get('buttons');
        var buttons = this.$('.asset');

        console.log('buttons', this.buttonData);
        console.log('Video Count:', buttons.length);

        var self = this;
        for(var i = 0; i < buttons.length; i++) {
            var button = $(buttons[i]);
            var rid = button.attr('id');
            console.log('looking for this id:', rid);
            var vid = this.getVideoByRazunaId(rid);

            var bkmrk = this.hasBookmark(vid.content);
            if(bkmrk) {
                bkmrk.done(function(bkm){
                    console.log('looky there... we found a bookmark');
                    var btn = self.$('#' + bkm.contentId);

                    btn.addClass('bookmark');
                    if(self.isPrescribed(bkm.orderID)) { btn.addClass('prescribed'); }
                    if(self.isCompleted(bkm.status)) { btn.addClass('completed'); }
                });
            } else {
                console.log('Nothing to see here folks. No bookmark for this vid.');
            }

            console.log('found vide', vid);

        }
    }
});

export default ContentButtonGroupCheckList;