import Pelican from 'pelican';
import  SetCenter from '../../helpers/setcenter';

require('./style.css');
var template = require('./template.hbs');

/**
 * Dialog is a widget you can throw into a screen that handles pop-up style modal dialog
 */
const LanguageDialog = Pelican.Dialog.extend({
    className: 'dialog language-dialog',
    template: template,

    events: {
        "click .language-select": "selectLanguage",
        'focus #spanish': 'highlightSpanish',
        'focus #english': 'highlightEnglish'
    },

    widgets: {
        buttons: {
            widgetClass: Pelican.ButtonGroup,
            selector: '#buttons'
        }
    },

    focusNext: function(){
        this.moveFocus('next');
        console.log('LanguageDialog.focusNext');
    },

    focusPrev: function(){
        this.moveFocus('prev');
        console.log('LanguageDialog.focusPrev');
    },

    selectLanguage: function (e) {
        App.data.epicResults.renditionlang = $(e.target).attr('id');
        var questionLocale = "";

        switch (App.data.epicResults.renditionlang) {
            case 'english':
            questionLocale = 'en';
                break;
            case 'spanish':
                questionLocale = 'es';
                break;
            default:
                questionLocale = 'en';
                break;
        }

        this.model.set('questionLocale', questionLocale);
        this.hide();
    },

    onShow: function(){
        var sc = SetCenter;
        sc.setCenter(this.$('.container'));
        this.getWidget('buttons').focus('#english');
    },

    highlightEnglish: function () {
        this.$('#instruction div').hide();
        this.$('#instruction div.english').show();
    },

    highlightSpanish: function () {
        this.$('#instruction div').hide();
        this.$('#instruction div.spanish').show();
    },

    hide: function(){
        LanguageDialog.__super__.hide.apply(this, arguments);

        this.onHide();
    },

    onHide: function(){},

    onRender: function(){
        var buttons = {
            buttons: [
                {id: 'english', text: 'English', className: 'menu-tab-button language-select'},
                {id: 'spanish', text: 'Español', className: 'menu-tab-button language-select'}
            ]
        };
        this.getWidget('buttons').model.set(buttons);
    }
});

export default LanguageDialog;

