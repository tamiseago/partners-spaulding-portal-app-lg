import Pelican from 'pelican';
import SetCenter from '../../helpers/setcenter';
import Intl from '../../helpers/intl';

require('./style.css');
var template = require('./template.hbs');

/**
 * Dialog is a widget you can throw into a screen that handles pop-up style modal dialog
 */
const LearnerDialog = Pelican.Dialog.extend({
    className: 'dialog welcome-viewer',
    template: template,

    events: {
        'click #next': 'onNext',
        'click .checkboxButton': 'selectLearner'
    },
    keyEvents: {},

    widgets: {
        buttons: {
            widgetClass: Pelican.ButtonGroup,
            selector: '#buttons'
        },
        buttonsRight: {
            widgetClass: Pelican.ButtonGroup,
            selector: '#buttons-right'
        }
    },

    onKey: function(e, key){
        var handled = false;

        switch(key) {
            case "UP":
                this.moveFocus('prev', $('#buttons > .button-group.widget'));
                handled = true;
                break;
            case "DOWN":
                this.moveFocus('next', $('#buttons > .button-group.widget'));
                handled = true;
                break;
            case "RIGHT":
                if(this.$('.checked').length > 0) {
                    $('#buttons > .button-group.widget a').removeClass('active');
                    this.focus('#next');
                }
                handled = true;
                break;
            case "LEFT":
                var isRight = $('#buttons-right > .button-group.widget a.active').length >= 1;
                if(isRight) {
                    $('#buttons > .button-group.widget a:first-child').addClass('active');
                    $('#next').removeClass('active');
                }
                handled = true;
                break;
            case "ENTER":
            case "OK":
            case "SELECT":
                this.click('.active');
                handled = true;
                break;
            default:
                handled = true;
                break;
        }

        return handled;
    },

    selectLearner: function (e) {
        console.log("LearnerDialog.selectLearner: have class checked? " + $(e.target).hasClass('checked'));
        if ($(e.target).hasClass('checked'))
            $(e.target).removeClass('checked');
        else
            $(e.target).addClass('checked');

    },

    onNext: function(){
        var learners = this.$('.checked');
        App.data.epicResults.learners = $.map(learners, function (l) {
            return $(l).attr('id');
        });
        this.hide();
    },

    onShow: function() {
        SetCenter.setCenter(this.$('.container'));
    },

    onRender: function () {
        var buttons = {
            buttons: [
                {id: 'patient', text: Intl.translate('The Patient'), className: 'checkboxButton active'},
                {id: 'family', text: Intl.translate('Family Member(s)'), className: 'checkboxButton'},
                {id: 'significant-other', text: Intl.translate('Significant Other'), className: 'checkboxButton'},
                {id: 'caregiver', text: Intl.translate('Caregiver(s)'), className: 'checkboxButton'},
                {id: 'mother', text: Intl.translate('Mother'), className: 'checkboxButton'},
                {id: 'father', text: Intl.translate('Father'), className: 'checkboxButton'},
                {id: 'guardian', text: Intl.translate('Guardian'), className: 'checkboxButton'},
                {id: 'foster-parent', text: Intl.translate('Foster Parent'), className: 'checkboxButton'}
            ]
        };

        var buttonsRight = {
            buttons: [
                {id: 'next', text: Intl.translate('Next'), className: 'mycare'}
            ]
        }

        this.getWidget('buttons').model.set(buttons);
        this.getWidget('buttonsRight').model.set(buttonsRight);
        
        // this.focus('#patient');

        console.log('LearnerDialog.display EXIT');

    },

    hide: function(){
        LearnerDialog.__super__.hide.apply(this, arguments);

        this.onHide();
    },

    onHide: function(){},
});

export default LearnerDialog;