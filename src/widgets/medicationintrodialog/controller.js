import Pelican from 'pelican';
import  SetCenter from '../../helpers/setcenter';
import PinInput from '../pininput/controller';
require('../../helpers/spatial_navigation.js');
require('../../css/dialog.css');
require('./style.css');
var template = require('./template.hbs');

/**
 * Dialog is a widget you can throw into a screen that handles pop-up style modal dialog
 */
const MedicationIntroDialog = Pelican.Dialog.extend({
    className: 'dialog medication-intro',
    template: template,

    events: {
    },

    widgets: {
        buttons: {
            widgetClass: Pelican.ButtonGroup,
            selector: '#buttons',
            options: { orientation: 'horizontal' }
        },
        pinInput: {
            widgetClass: PinInput,
            selector: '#pin-input'
        }
    },

    onKey: function(e, key){
        e.preventDefault();
        e.stopImmediatePropagation();
        console.log('MedicationIntroDialog.onKey ' + key);
        var handled = false;
        var focus = this.model.get('hasFocus');
        // if(!focus)
        //     return false;
        
        var nums = [1,2,3,4,5,6,7,8,9,0];
        var lurd = ["LEFT", "UP", "RIGHT", "DOWN"];
        var select = ["SELECT", "OK", "ENTER"];
        var widget = false;

        if($.inArray(parseInt(key), nums) > -1) {
            var widget = this.getWidget('pinInput');
            widget.focus();
            handled = widget.onKey(e, key);
        } else if($.inArray(key, lurd) > -1) {
            this.moveFocus('next', this.$('#buttons > div'));
            handled = true;
        } else if($.inArray(key, select) > -1) {
            this.click(this.$('#buttons > div a.active')[0])
            handled = true;
        }
        
        return handled;
    },

    onInit: function(){
        this.parent = this.model.get('parent');
    },

    onClose: function(){
        var parent = this.model.get('parent');
    },

    onShow: function(){
        var sc = SetCenter;
        sc.setCenter(this.$('.container'));
        var pi = this.getWidget('pinInput');
        var btn = this.getWidget('buttons');

        var self = this;
        pi.model.on('change pin', function(e){
            console.log('PIN HAS CHANGED ' +
            '\npin: ' + pi.model.get('pin') +
            '\nactive pin: ' + pi.activePin);
            if(pi.activePin == 'pin4') {
                btn.focus()
            }
        })
        
        pi.focus('#pin1');
        btn.focus('#accept');
    },

    onRender: function () {
        var parent = this.model.get('parent');

        var model = {
            buttons: [
                {id: 'accept', text: i18n.t('Accept')},
                {id: 'decline', text: i18n.t('Decline')}
            ]
        };

        this.getWidget('buttons').model.set(model);

    },

    hide: function(){
        MedicationIntroDialog.__super__.hide.apply(this, arguments);
        this.model.set('hasFocus', false);

        this.onHide();
    },

    onHide: function(){},
});

export default MedicationIntroDialog;