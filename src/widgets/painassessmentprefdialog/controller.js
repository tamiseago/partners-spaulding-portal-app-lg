import Pelican from 'pelican';
import  SetCenter from '../../helpers/setcenter';
import Intl from '../../helpers/intl';
require('../../css/dialog.css');
require('./style.css');
var template = require('./template.hbs');

/**
 * Dialog is a widget you can throw into a screen that handles pop-up style modal dialog
 */
const PainPrefDialog = Pelican.Dialog.extend({
    className: 'dialog painpref-dialog',
    template: template,

    events: {
        "click .painpref-select": "selectPainPref",
        'focus #yes': 'highlightYes',
        'focus #no': 'highlightNo'
    },

    widgets: {
        buttons: {
            widgetClass: Pelican.ButtonGroup,
            selector: '#buttons'
        }
    },

    focusNext: function(){
        this.moveFocus('next');
        console.log('PainPrefDialog.focusNext');
    },

    focusPrev: function(){
        this.moveFocus('prev');
        console.log('PainPrefDialog.focusPrev');
    },

    selectPainPref: function (e) {
        App.data.painPref = $(e.target).attr('id');
        var questionPain = "";

        switch (App.data.painPref) {
            case 'yes':
                questionPain = 'yes';
                break;
            case 'no':
                questionPain = 'no';
                break;
            default:
                questionPain = 'yes';
                break;
        }

        this.model.set('questionPain', questionPain);
        this.hide();
    },

    onShow: function(){
        var sc = SetCenter;
        sc.setCenter(this.$('.container'));
        this.getWidget('buttons').focus('#yes');
    },

    highlightYes: function () {
        this.$('#instruction div').hide();
        this.$('#instruction div.yes').show();
    },

    highlightNo: function () {
        this.$('#instruction div').hide();
        this.$('#instruction div.no').show();
    },

    hide: function(){
        PainPrefDialog.__super__.hide.apply(this, arguments);

        this.onHide();
    },

    onHide: function(){},

    onRender: function(){
        var buttons = {
            buttons: [
                {id: 'yes', text: 'Thank you  for letting me know', className: 'menu-tab-button painpref-select'},
                {id: 'no', text: 'No, do not send Pain Assessments', className: 'menu-tab-button painpref-select'}
            ]
        };
        this.getWidget('buttons').model.set(buttons);
    }
});

export default PainPrefDialog;

