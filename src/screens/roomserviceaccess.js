import Pelican from 'pelican';
import Upserver from 'upserver-client';

require('../css/rs-secondary.css');
require('../css/roomservice.css');
var template = require('../templates/roomservice.hbs');

const RoomServiceAccess = Pelican.Screen.extend({

    className: 'secondary',

    template: template,

    widgets: {
        dialog: {
            widgetClass: Pelican.Dialog,
            selector: '#dialog',
            options: {hidden: true}
        },
    },

    regions: {
        'title': {
            selector: '.page-title'
        }
    },

    keyEvents: {},

    events: {
        'click #close': 'selectClose',
        'click .back-button': 'back',
        'focus .menu-tab-button': 'menuFocused',
        'click #next': 'selectMyMenu'
    },


    onInit: function (options) {
        var slug = this.path.replace('home/', App.upserver.baseSlug);
        console.log('Slug: ' + this.path);

        var self = this;


        var contents = new Pelican.Collections.ContentCollection();
        contents.fetch({
            data: {
                slug: slug,
                depth: 2
            }
        })
            .done(function () {
                console.log('APP ROUTE SEGMENT: ' + slug);
                var content = contents.toJSON();
                var model = {menu: content};
                self.showComingSoon();
            });



    },


    showComingSoon : function () {
        var self = this;

        var p1 = i18n.t('Room Service');
        var p2 = i18n.t('Room Service is currently<br> not available.') + '<br>';
        var p3 = i18n.t('Please check back soon!');
        var p4 = i18n.t('For ordering, please ask your <br>nurse for a paper menu.');

        var html =
            '<div class="english regtext">' +
            '<p>' + p1 + '</p><br>' +
            '</div><div class="regtext2">' +
            '<p>' + p2 + '</p><br>' +
            '<p>' + p3 + '</p><br>' +
            '<p>' + p4 + '</p><br>' +
            '</div>';


        var model = {
            buttons: [
                {id: 'close', text: i18n.t('Close'), className: "bottom"},
            ],
            content: html
        };
        self.getWidget('dialog').model.set(model);
        self.getWidget('dialog').show();

        self.focus('#close');
    },

    selectClose: function () {
        this.back()
    },

    onAttach: function () {
    },

    selectVisitorMenu: function () {

    },

    validatePatron: function () {

    },

    selectMyMenu: function () {
        console.log('gotomymenu Slug: ' + this.path);
        this.getPatron('mymenu');
    },

    getPatron: function (type) {

        var self = this;
        var defer = jQuery.Deferred();

        if (App.data && App.data.patient && App.data.patient.patientProfile) {
            if (App.data && App.data.device && App.data.device.deviceProvision && App.data.device.deviceProvision.location) {
                var location = App.data.device.deviceProvision.location;
            }
            var fullname 	= '?name='+App.data.patient.patientProfile.fullName;
            var room 	= 'room='+location.room;
            var bed  	= 'bed='+location.bed;
            var mac  	= 'mac='+App.data.device.id;
            var url = App.config.mirth.host + ":" + App.config.mirth.getPatronPort +  fullname+'&'+room+'&'+bed+'&'+mac;

            App.upserver.api('/proxy', 'GET', {"url": url})
                .done(function (data) {
                    if (!data || data.length < 1) {
                        defer.resolve('');
                    }
                    var success = 0;
                    var errormsg = '';

                    $(data).find('STATUS').each(function(){
                        success = $(this).attr('success');
                        if(success=='0')
                            $(data).find('ERROR').each(function(){
                                errormsg = $(this).attr('text');
                            });
                    });

                    console.log(errormsg);
                    defer.resolve(success);
                })
                .fail(function (f) {
                    defer.reject(f.errorMessage);
                });

            return defer.promise();

        }
    },

    openDineError: function () {

    },

    menuFocused: function (e) {
        var $item = $(e.target);
        var path = $item.attr('data-slug').replace(App.upserver.baseSlug, 'home/');
        var id = $item.attr('data-current-slug').replace(/[\.:#]/g, '-');;
        if(path) {
            App.tracker.pageView(path, id);
        }
    }
});

export default RoomServiceAccess;