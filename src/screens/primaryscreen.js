import Pelican from 'pelican';
import ApiHelper from '../helpers/apihelper';
import LanguageDialog from '../widgets/languagedialog/controller';
import PainPrefDialog from '../widgets/painassessmentprefdialog/controller';

var packageJson = require("../../package.json");

require('../css/common.css');
require('../css/dialog.css');
require('../css/dialog-popup.css');
require('../css/primary.css');

var template = require('../templates/primaryscreen.hbs');

const PrimaryScreen = Pelican.Screen.extend({

    className: 'primary',

    template: template,

    keyEvents: {
        'POWER': true,
        'EXIT': true,
        'BACK': true,
        'HOME': true,
        'CLOSE': true,
        'MENU': false
    },

    events: {
        'click #pain-assessment': 'selectPainPref',
        'click #my-language': 'selectMyLanguage',
        // language dialog
        'focus #lang-english': 'highlightEnglish',
        'focus #lang-spanish': 'highlightSpanish',
        'click #lang-english': 'selectLanguage',
        'click #lang-spanish': 'selectLanguage'
    },

    widgets: {
        dialog: {
            widgetClass: Pelican.Dialog,
            selector: '#dialog',
            options: {hidden: true}
        },
        languageDialog: {
            widgetClass: LanguageDialog,
            selector: '#languageDialog',
            options: { hidden: true }
        },
        painPrefDialog: {
            widgetClass: PainPrefDialog,
            selector: '#painPrefDialog',
            options: { hidden: true }
        },
        mainmenu: {
            widgetClass: Pelican.DropdownMenu,
            selector: '#menu'
        },
        datetime: {
            widgetClass: Pelican.DigitalClock,
            selector: '#date',
            options: {format: 'h:MM TT, dddd, mmmm d, yyyy', locale: 'en'}
        }
    },

    onInit: function (options) {
        var slug = App.upserver.baseSlug.substr(0, App.upserver.baseSlug.length - 1);
        var self = this;

        // dynamic set clock language
        self.getWidget('datetime').locale = App.data.language;
        this.getWidget('languageDialog').model.set('parent', this);
        this.getWidget('painPrefDialog').model.set('parent', this);

        // get menu from server
        var contents = new Pelican.Collections.ContentCollection();
        console.log('getting content from: ' + slug);
        contents.fetch({
            data: {
                slug: slug,
                depth: 2
            }
        })
            .done(function () {
                var content = contents.toJSON();
                var model = {menu: content};
                self.getWidget('mainmenu').model.set(model);
                self.click('#menu1');
            })
            .fail(function () {
                self.$('.major').html('<p>Unable to get menu content from server.  Please contact your nurse.</p>').css('text-align', 'center');
            });
    },

    updatePatient: function () {
        if (App.data && App.data.patient && App.data.patient.patientProfile) {
            this.$('#patient-name').text(App.data.patient.patientProfile.fullName);
        }
    },

    updateRoom: function () {
        if (App.data && App.data.device && App.data.device.deviceProvision && App.data.device.deviceProvision.location) {
            var location = App.data.device.deviceProvision.location;

            this.$('#room-phone').text(location.phone);
            var room = location.room;
            if (location.bed != '00') {
                room = ' Room ' + location.room;
            }
            this.$('#room-number').text(room);
        }
    },

    onAttach: function () {
        var self = this;

        this.updatePatient();
        this.updateRoom();
        upserver.on(upserver.events.patient.profileChanged, self.updatePatient.bind(this));
        upserver.on(upserver.events.device.locationUpdated, self.updateRoom.bind(this));

        var pathnames = location.pathname.split('/');
        var appName = pathnames[pathnames.length - 2] || '';
        this.$('#app-version').text(appName + ' ' + packageJson.version);

    },

    onScreenShow: function() {
        $('body').removeClass('video');
    },

    selectPainPref: function (e) {
        // cancel the link
        e.preventDefault();
        e.stopImmediatePropagation();

        // show dialog
        // this.getWidget('dialog').show();
        this.getWidget('painPrefDialog').show();

        var currPainPref = App.data.painPref;
        if (currPainPref == 'yes') {
            this.focus('#yes');
        }
        else {
            this.focus('#no');
        }
    },

    showPainPrefDialog: function () {
        var wgtPainPref = this.getWidget('painPrefDialog');
        wgtPainPref.show();
        console.log('primary.showPainPref END');
    },

    selectMyLanguage: function (e) {
        // cancel the link
        e.preventDefault();
        e.stopImmediatePropagation();

        // show dialog
        // this.getWidget('dialog').show();
        this.getWidget('languageDialog').show();

        var currLang = App.data.language;
        if (currLang == 'es') {
            this.focus('#lang-spanish');
        }
        else {
            this.focus('#lang-english');
        }
    },

    onLanguageSelection: function(){},

    highlightEnglish: function () {
        this.getWidget('dialog').$('#instruction div').hide();
        this.getWidget('dialog').$('#instruction div.english').show();
    },

    highlightSpanish: function () {
        this.getWidget('dialog').$('#instruction div').hide();
        this.getWidget('dialog').$('#instruction div.spanish').show();
    },

    selectLanguage: function (e) {
        var self = this;
        var id = $(e.target).attr('id');
        var lang = 'en';
        if (id == 'lang-spanish') {
            lang = 'es';
        }

        this.getWidget('dialog').hide();

        if (App.data.language != lang) {
            // change langauge. reload home page
            App.data.language = lang;   // global option
            window.upserver.setLanguage(lang);  // set client API cookie
            window.upserver.locale = lang;  // set client API language header
            ApiHelper.setPreference('language', lang); // save to server

            // load new resource
            i18next.changeLanguage(lang, function () {
                App.layout.closeAllScreens();
                // this is a trick to force it go to '#home' even URL is already '#home'
                Backbone.history.loadUrl(Backbone.history.fragment);
            });

            // analytics
            App.tracker.event('language', 'switch', lang);
        }
    }
});

export default PrimaryScreen;
