import Pelican from 'pelican';
import ApiHelper from '../helpers/apihelper';

require('../css/common.css');
require('../css/edlibrary-synopsis.css');

var template = require('../templates/edsynopsisscreen.hbs');

const EDSynopsisScreen = Pelican.Screen.extend({

    className: 'edsynopsis',

    video: new Pelican.Models.Content(),

    template: template,

    creditLength: 10,

    keyEvents: {},

    events: {
        'focus .back-button': 'focusBackButton',
        'focus #play' : 'focusButton',
        'focus #restart' : 'focusButton',
        'focus #resume' : 'focusButton',
        'focus #remove' : 'focusButton',
        'focus #add' : 'focusButton',
        'click .back-button': 'back',
        'click #add': 'addBookmark',
        'click #remove': 'removeBookmark'
    },

    widgets: {

        buttons: {
            widgetClass: Pelican.ButtonGroup,
            selector: '#buttons',
            options: {orientation: 'vertical', preRender: true}
        }

    },

    calcDuration: function (duration) {
        if (!duration)
            return '00:00';
        var str = '';
        if (duration >= 3600) {
            var h = Math.floor(duration / 3600);
            str += h + ':';
            duration = duration % 3600;
        }
        if (duration >= 60) {
            var mm = Math.floor(duration / 60);

            str += mm + ':';
            duration = duration % 60;
        }
        var ss = duration;
        str += (ss < 10 ? '0' : '') + ss;
        return str;
    },

    getVideo: function (videoId) {
        var self = this;
        var defer = jQuery.Deferred();

        var vid = videoId || self.vid;

        self.video.fetch({
            uri: '/contents',


            data: {
                id: vid
            }
        })
            .done(function () {
                self.state = 'loaded';
                console.log(self.video);
                defer.resolve();
            })
            .fail(function (f) {
                defer.reject(f.errorMessage);
            });

        return defer.promise();
    },


    onInit: function () {
        var self = this;
        console.log('video queries: ' + JSON.stringify(self.queries));
        $('body').addClass('video');

        if (self.params.length > 1) {
            self.vid = self.params[0];
        }

        if (self.queries && self.queries['vid']) {
            self.vid = self.queries['vid'];
        }

        console.log('vid = ' + self.vid);

    },


    onScreenShow: function () {

        var self = this;
        self.getBookmark();
    },

    onAttach: function () {
    },

    getBookmark: function () {
        var self = this;
        var defer = $.Deferred();
        self.bookmark = undefined;
        self.bookmarkId = undefined;
        self.bookmarkIds = undefined;
        self.orderID = undefined;
        self.title = '';
        self.lastPosition = 0;
        self.getVideo()
            .done(function () {

                if (self.video && self.video.attributes) {
                    self.bookmarkIds = self.video.attributes.bookmarkIds;
                    self.title = self.video.attributes.metadatas.title || self.video.get('metadatas').title || self.video.attributes.name || '';
                    self.bookmarkId = self.queries['bookmarkId'] || self.video.attributes.bookmarkIds[0];
                    self.runTime = self.video.attributes.metadatas.runtime || 0;
                }

                self.lastPosition = 0;
                self.orderID = undefined;

                self.duration = self.calcDuration(self.runTime);
                // var heading = self.title + "<br>" + self.duration + ' mins'
                console.log('EDSynopsisScreen.getBookmark: self.bookmarkId ' + self.bookmarkId);
                console.log('EDSynopsisScreen.getBookmark: self.bookmarkIds ');
                console.log(JSON.stringify(self.bookmarkIds));


                if (self.bookmarkId) {
                    var bookmark = new Pelican.Models.Bookmark({id: self.bookmarkId});
                    bookmark.fetch()
                        .done(function () {
                            console.log(bookmark);
                            self.bookmark = bookmark;

                            console.log('EDSynopsisScreen.getBookmark: self.bookmark ');
                            console.log(JSON.stringify(self.bookmark));

                            self.lastPosition = bookmark.attributes.lastPosition;
                            self.orderID = bookmark.attributes.orderID;
                            self.title = self.video.attributes.metadatas.title || self.video.get('metadatas').title || self.video.attributes.name || '';
                            self.updateBookmarkStatus();
                            defer.resolve();
                        })
                        .fail(function () {
                            self.updateBookmarkStatus();
                            defer.reject();
                        });
                }
                else {
                    self.updateBookmarkStatus();
                    defer.resolve();
                }



                self.$('#heading1 #vidTitle').text(self.title);
                self.$('#heading1 #vidDuration').text(self.duration + ' mins.');

            });

        return defer.promise();
    },


    updateBookmarkStatus: function () {
        var self = this;
        self.$('#buttons a.btn').hide();


        if (this.bookmarkId) {
            self.$('a#play').show().attr('href', '#educationvideo/' + self.params[0] + '?bookmarkId=' + self.bookmarkId);
            self.$('a#restart').show().attr('href', '#educationvideo/' + self.params[0] + '?bookmarkId=' + self.bookmarkId + '&start=0');
            self.$('a#resume').show().attr('href', '#educationvideo/' + self.params[0] + '?bookmarkId=' + self.bookmarkId + '&start=' + self.lastPosition * 1000);
            if (!this.orderID) {
                self.$('a#play').show().attr('href', '#educationvideo/' + self.params[0] + '?bookmarkId=' + self.bookmarkId);
                self.$('a#remove').show();  // only self-bookmarked video can be removed
                // non-prescribed video should not pop questions, hence use EducationVideoPlayer instead of EpicVideoPlayer
                self.$('a#restart').attr('href', '#educationvideo/' + self.params[0] + '?bookmarkId=' + self.bookmarkId + '&start=0');
                self.$('a#resume').attr('href', '#educationvideo/' + self.params[0] + '?bookmarkId=' + self.bookmarkId + '&start=' + self.lastPosition * 1000);
            }
            self.$('.minor .sub-text1 .bookmarked').show();


            self.blur();

        }
        else {
            self.$('a#play').show().attr('href', '#educationvideo/' + self.params[0] + '?bookmarkId=' + self.bookmarkId);
            self.$('a#add').show();
            self.$('.minor .sub-text1 .not-bookmarked').show();

            self.blur();
            self.focus(self.$('a#play'));
        }

        self.bookmark = self.bookmark || {attributes:{content:{metadatas:{}}}};
        self.lastPosition = self.bookmark.attributes.lastPosition || 0;
        var lpWithCredits = self.creditLength + self.lastPosition;
        var rt = ApiHelper.calcDurationInSeconds(self.bookmark.attributes.content.metadatas.runtime);


        console.log('EDSynopsisScreen.updateBookmarkStatus: this.bookmarkId ' + this.bookmarkId);
        /**
         * video statuses:
         * "NotStarted", "InProgress", "Completed"
         * This was a quick hack to handle the show/hide of
         * particualar buttons. This shoudl be cleaned up. {GS}
         */
        var status = "NotStarted";

        if(self.bookmark && self.lastPosition > 0) {
            if(lpWithCredits >= rt){
                status = "Completed";
            } else if(lpWithCredits < rt) {
                status = "InProgress";
            } else {
                status = self.bookmark.attributes.status;
            }
        }

        console.log('EDSynopsisScreen.onScreenShow: video.attributes.metadatas.runtime ' + self.video.attributes.metadatas.runtime)
        console.log('EDSynopsisScreen.onScreenShow: lastPosition ' + self.lastPosition);
        console.log('EDSynopsisScreen.onScreenShow: lpWithCredits ' + lpWithCredits);
        console.log('EDSynopsisScreen.onScreenShow: bookmark...runtime ' + rt);
        console.log('EDSynopsisScreen.onScreenShow: duration ' + self.duration);
        console.log('EDSynopsisScreen.updateBookmarkStatus: bookmark status ' + status);
        //hide text and buttons
        self.$('.minor .sub-text1 .add').hide();
        self.$('.minor .sub-text1 .play').hide();
        self.$('.minor .sub-text1 .remove').hide();
        self.$('.minor .sub-text1 .resume').hide();
        self.$('.minor .sub-text1 .restart').hide();

        self.$('a#add').hide();
        self.$('a#play').hide();
        self.$('a#remove').hide();
        self.$('a#resume').hide();
        self.$('a#restart').hide();

        switch(status){
            case 'NotStarted':
                self.$('a#play').show();
                self.$('.minor .sub-text1 .play').show();
                if(!self.orderID) {//show add or remove bookmark
                    if(this.bookmarkId) {
                        self.$('a#remove').show();
                        self.$('.minor .sub-text1 .remove').show();
                    } else {
                        self.$('a#add').show();
                        self.$('.minor .sub-text1 .add').show();
                    }
                }

                self.focus($('a#play'));

                break;
            case 'InProgress':

                self.$('a#restart').show();
                self.$('.minor .sub-text1 .restart').show();
                self.$('a#resume').show();
                self.$('.minor .sub-text1 .resume').show();
                if(!self.orderID) {//show add or remove bookmark
                    self.$('a#remove').show();
                    self.$('.minor .sub-text1 .remove').show();
                }
                self.focus($('a#resume'));
                break;
            case 'Completed':
                self.$('a#restart').show();
                self.$('.minor .sub-text1 .restart').show();
                if(!self.orderID) {//show add or remove bookmark
                    self.$('a#remove').show();
                    self.$('.minor .sub-text1 .remove').show();
                }
                self.focus($('a#restart'));
                break;
        }
    },

    focusBackButton: function (e) {
        this.$('.minor .menu-tab').hide();
    },

    focusButton: function (e){
        this.$('.minor .menu-tab').show();
    },

    addBookmark: function () {
        var self = this;
        if (!this.bookmarkId) {
            var nbm = new Pelican.Models.Bookmark();
            nbm.save({
                contentId: self.vid,
                type: 'education'
            })
                .done(function () {
                    console.log('Bookmark created');
                    self.getBookmark();
                    self.updateBookmarkStatus();
                    // analytics
                    App.tracker.event('edvideo', 'bookmark', 'manualadd');
                    self.back();
                });

        }
    },

    removeBookmark: function () {
        var self = this;
        if (self.bookmark) {
            self.bookmark.destroy()
                .done(function () {
                    console.log('Bookmark deleted');
                    self.getBookmark();
                    self.updateBookmarkStatus();
                    // analytics
                    App.tracker.event('edvideo', 'bookmark', 'manualremove');

                    self.back();
                });

        }
    }

});

export default EDSynopsisScreen;
