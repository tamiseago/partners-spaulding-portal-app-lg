import Pelican from 'pelican';

require('../css/sleep-enhancement.css');
var template = require('../templates/sleepenhancement.hbs');

const SleepEnhancementScreen = Pelican.Screen.extend({

    className: 'sleepenhancement',

    template: template,

    regions: {
        'title': {
            selector: '.page-title'
        }
    },

    keyEvents: {},

    events: {
        'click .back-button': 'back',
        'focus .back-button' : 'backFocused',
        'focus .menu-tab-button': 'menuFocused'
    },

    widgets: {
        sleep: {
            widgetClass: Pelican.TabMenu,
            selector: '#menu'
        }
    },

    onInit: function (options) {
        var slug = this.path.replace('home/', App.upserver.baseSlug);
        console.log('Slug: ' + slug);

        var self = this;

        // get self content
        var parent = new Pelican.Models.Content();
        parent.fetch({
            data: {
                slug: slug
            }
        }).done(function () {
            var pcont = parent.toJSON();
            self.$('.page-title').first().html(pcont.content.metadatas.title);
            if(pcont.content.metadatas.htmlHeading2)
                self.$('#heading2 p').first().html(self.decodeHTML(pcont.content.metadatas.htmlHeading2));
            self.getWidget('sleep').model.set({parent: pcont.content});

            var contents = new Pelican.Collections.ContentCollection();
            contents.fetch({
                data: {
                    slug: slug,
                    depth: 2
                }
            }).done(function () {
                console.log('APP ROUTE SEGMENT: ' + slug);
                var scont = contents.toJSON();
                var model = {menu: scont};
                self.getWidget('sleep').model.set(model);
                //uses special coding becuase these menu items are actions in a folder
                if(pcont.content.metadatas.htmlText1) {
                    self.$('.menu-tab .sub-text1').html('<p>' +self.decodeHTML(pcont.content.metadatas.htmlText1) + '</p>');
                }
                self.focus('#menu1');
                self.getWidget('sleep').showTab($('#menu1'));


            });
        });

    },

    onAttach: function () {

    },

    menuFocused: function (e) {
        self.$('.sub-content').show();
        var $item = $(e.target);
        var path = $item.attr('data-slug').replace(App.upserver.baseSlug, 'home/');
        var id = $item.attr('data-current-slug').replace(/[\.:#]/g, '-');;
        if(path) {
            App.tracker.pageView(path, id);
        }
    },

    backFocused: function () {
        self.$('.sub-content').hide();
    } ,

    decodeHTML : function (html) {
        var txt = document.createElement('textarea');
        txt.innerHTML = html;
        return txt.value;
    },



});

export default SleepEnhancementScreen;
