import Pelican from 'pelican';
import ContentButtonGroupCheckList from "../widgets/contentbuttongroupchecklist/controller";
require('../css/common.css');
require('../css/edlibrary.css');
var template = require('../templates/recoverystoriesscreen.hbs');

const RecoveryStoriesScreen = Pelican.Screen.extend({

    className: 'recovery edtitles',

    template: template,

    keyEvents: {},

    events: {
        'focus .back-button': 'focusBackButton',
        'focus a.ellipsis' : 'focusAsset',
        'click .back-button': 'back'
    },

    widgets: {
        recoverystories: {
            widgetClass: ContentButtonGroupCheckList,
            selector: '.major',
            options: {backButton: true, nonActive: false, contentClass: 'asset'}
        }
    },

    onInit: function (options) {
        var slug = this.path.replace('home/', App.upserver.baseSlug);
        console.log('Slug: ' + this.path);
        var gslug = slug.substring(0, slug.lastIndexOf("/"));
        var self = this;

        var parent = new Pelican.Models.Content();
        parent.fetch({
            data: {
                slug: slug
            }
        }).done(function () {
            var pcont = parent.toJSON();
            self.$('.page-title').first().html(pcont.content.metadatas.title);
        });

        var contents = new Pelican.Collections.ContentCollection();
        contents.fetch({
            data: {
                slug: slug,
                depth: 2
            }
        }).done(function () {
            var scont = contents.toJSON();
            var bmod = {buttons: scont};
            self.getWidget('recoverystories').model.set(bmod);
            if(scont.length>=1) {
                self.focus(self.$('.ellipsis').first());
                self.focusAsset();
            } else {
                self.getWidget('bookmarks').selectFirst();
                self.focusBackButton();
            }
        });

    },

    focusAsset: function (e) {
        this.$('.minor .menu-tab').show();
        this.$('.minor .back-tab').hide();
    },

    focusBackButton: function (e) {
        if(this.$('.ellipsis').length>=1) {
            this.$('.minor .menu-tab').hide();
            this.$('.minor .back-tab').hide();
        } else {
            this.$('.minor .menu-tab').hide();
            this.$('.minor .back-tab').show();
        }

    }

});

export default RecoveryStoriesScreen;
