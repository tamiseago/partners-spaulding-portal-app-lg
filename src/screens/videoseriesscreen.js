import Pelican from 'pelican';
import MovieScreen from './moviescreen';

require('../css/movies.css');

const VideoSeriesScreen = MovieScreen.extend({

    getThumbnail: function(series){

        console.log('VideoSeriesScreen.getThumbnail.series:');
        console.log(series);
        
        for(var i = 0; i < series.length; i++) {
            var s = series[i];
            s.content.thumbnail = s.children[0].content.thumbnail;
            series[i] = s;
        }
        return series;
    },

    onInit: function (options) {
        var self = this;
        var folders = new Pelican.Collections.ContentCollection();
        var slug = self.path.replace('home/', App.upserver.baseSlug);
        console.log('VideoSeriesScreen.onInit.slug: ' + slug);

        folders.fetch({
            data: {
                slug: slug,
                depth: 3
            }
        })
            .done(function () {
                console.log('Movie ROUTE SEGMENT: ' + slug);
                var content = folders.toJSON();
                content[0].children = self.getThumbnail(content[0].children);
                console.log('VideoSeriesScreen.onInit.content');
                console.log(content);
                self.movies = content;

                /**
                 * This is down and dirty. Should be done more neat.
                 * Depending on portal, path index could be different.
                 */
                var screen = self.path.split('/')[2];
                console.log('VideoSeriesScreen.onInit.screen: ' + screen);
                var i18t = '';
                var i18header = '';
                if(screen == 'free-movies'){
                    i18t = 'Free Movies';
                } else if(screen == 'free-movies-spanish'){
                    if(App.data.language == 'en') {
                        i18t = 'Free Movies - Spanish';
                        i18header = 'Use Right / Left arrow keys to scroll the movies'
                    } else if(App.data.language == 'es') {
                        i18t = 'Free Movie - Spanish';
                        i18header = 'Use Right / Left arrow keys to scroll the movies'
                    }
                } else if(screen == 'tv-comedies') {
                    i18t = 'TV Comedies';
                    i18header = '<br>Use Right / Left arrow keys to scroll the comedies'
                }

                self.$('.page-title').first().html(i18n.t(i18t));
                self.$('#heading2 p').first().html(i18n.t(i18header));
                self.getWidget('library').model.set('folders', content);
                self.getWidget('library').selectFirstFolder();
            });

    },

    onRender: function(){
        var posters = this.getWidget('library')
            .getWidget('assets');
        posters.contentUrlPrefix = 'home/watch-tv/tv-comedies/tv-comedies';
    }
});

export default VideoSeriesScreen;
