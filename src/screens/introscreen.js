import Pelican from 'pelican';
import ApiHelper from '../helpers/apihelper';

require('../css/global.css');
require('../css/common.css');
require('../css/intro.css');
require('../css/intro.override.css');

var template = require('../templates/introscreen.hbs');

const IntroScreen = Pelican.Screen.extend({

    className: 'intro',

    template: template,

    keyEvents: {
        'POWER': true,
        'EXIT': true,
        'BACK': true,
        'HOME': true,
        'CLOSE': true
    },

    events: {
        'click #select': 'pressSelect'
    },

    redirectOnLoadIfComplete: true,
    widgets: {},
    bookmarks: [],

    hasCompletedWelcomeVideo(wvid, bookmarks) {
        for(var i = 0; i < bookmarks.length; i++) {
            var b = bookmarks[i];
            if(b.contentId == wvid && b.status == 'Completed') {
                return true;
            }
        }

        return false;
    },

    getBookmarks: function(){
        var defer = jQuery.Deferred();

        upserver.api('/me/bookmarks')
            .done(function(bookmarks){
                console.log('IntroScreen.pressSelect.bookmarks');
                console.log(bookmarks);
                defer.resolve(bookmarks);
            });

        return defer.promise();
    },

    getWelcomeBookmark: function(wvid, bookmarks){
        for(var i = 0; i < bookmarks.length; i++) {
            var b = bookmarks[i];
            if(b.contentId == wvid) {
                return b;
            }
        }
    },

    goToWelcomeVideo: function(wvid, bkmrk){
        var start = 0;
        if(bkmrk) {
            start = bkmrk.lastPosition * 1000;
        }
        var vurl = "#educationvideo/" + wvid + '?start=' + start;
        console.log('IntroScreen.onInit: we re starting th welcome video at: ' + vurl);
        App.router.go(vurl);
    },

    onScreenShow: function(){
        var self = this;

        console.log('IntroScreen.onScreenShow');
        this._redirectToPortal()
            .done(function(redirect){
                if(redirect) {
                    console.log('IntroScreen.onScreenShow: going home.');
                    App.layout.closeAllScreens();
                    App.router.go('home');
                } else {
                    console.log('IntroScreen.onScreenShow: gotta stay here.');
                    self.focus("a#select");
                }
            });
    },

    onInit: function(){
        var self = this;
    },

    pressSelect: function() {
        var self = this;
        var wvid = App.config.welcomeVideo;

        var completed = self.hasCompletedWelcomeVideo(wvid, self.bookmarks);
        console.log('IntroScreen.pressSelect.completed: ' + completed);
        if(completed) {
            this.keyEvents.MENU = false;
            App.layout.closeAllScreens();
            App.router.go('home');
        } else {
            var bkmrk = self.getWelcomeBookmark(wvid, self.bookmarks);
            self.goToWelcomeVideo(wvid, bkmrk);
        }
    },

    _redirectToPortal: function(){
        var self = this;
        var defer = jQuery.Deferred();
        var wvid = App.config.welcomeVideo;

        this.getBookmarks()
            .done(function(bkmrks){
                self.bookmarks = bkmrks;
                var completed = self.hasCompletedWelcomeVideo(wvid, self.bookmarks);
                console.log('IntroScreen.pressSelect.completed: ' + completed);
                if(completed && self.redirectOnLoadIfComplete) {
                    defer.resolve(true);
                } else {
                    console.log('IntroScreen.pressSelect: gotta stay here.');
                    defer.resolve(false);
                    self.focus("a#select");
                }
            });
        return defer.promise();
    }

});

export default IntroScreen;
