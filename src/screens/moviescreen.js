import Pelican from 'pelican';

require('../css/movies.css');

var template = require('../templates/moviescreen.hbs');

const MovieScreen = Pelican.Screen.extend({

    className: 'movies',

    template: template,

    keyEvents: {},

    events: {
        'click .back-button': 'back',
        'click .folder': 'selectFolder',
        'focus #menu a': 'bbFocus'
    },

    widgets: {
        library: {
            widgetClass: Pelican.ContentCarousel,
            selector: '#menu',
            options: {}
        }
    },

    movies: [],

    onInit: function (options) {
        var self = this;
        var folders = new Pelican.Collections.ContentCollection();
        var slug = self.path.replace('home/', App.upserver.baseSlug);
        console.log('Slug: ' + self.path);

        folders.fetch({
            data: {
                slug: slug,
                depth: 2
            }
        })
            .done(function () {
                console.log('Movie ROUTE SEGMENT: ' + slug);
                var content = folders.toJSON();
                console.log('MovieScreen.onInit.content', content);
                self.movies = content;

                /**
                 * This is down and dirty. Should be done more neat.
                 * Depending on portal, path index could be different.
                 */
                var screen = self.path.split('/')[2];
                console.log('SCREEN: ' + screen);
                var i18t = '';
                if(screen == 'free-movies'){
                    i18t = 'Free Movies';
                } else if(screen == 'free-movies-spanish'){
                    if(App.data.language == 'en') {
                        i18t = 'Free Movies - Spanish';
                    } else if(App.data.language == 'es') {
                        i18t = 'Free Movie - Spanish';
                    }
                }
                self.$('.page-title').first().html(i18n.t(i18t));

                self.getWidget('library').model.set('folders', content);
                self.getWidget('library').selectFirstFolder();
            });

    },

    onAttach: function () {

    },

    selectFolder: function (e) {
        var $obj = $(e.target);
        var slug = $obj.attr('data-slug');

        var self = this;
        var content = [];

        console.log('MovieScreen.selectFolderselectFolder');

        /* var assets = new Pelican.Collections.ContentCollection();
        assets.fetch({
            data: {
                slug: slug,
                depth: 1
            }
        })
            .done(function () {
                content = assets.toJSON();
            })
            .always(function () {
                self.getWidget('library').model.set('assets', content);
            }); */

        
            var ct = this.$(e.currentTarget);
            var lib = this.getWidget('library');
            var cslug = ct.attr('data-current-slug');
            var children = this.getMoviesFromFolder(cslug);
            console.log('MovieScreen.bbFocus.cslug:', cslug);
            console.log('MovieScreen.bbFocus.children:', children);
            lib.model.set('assets', children);
    },

    getMoviesFromFolder: function(cslug){
        var movies = this.movies;
        for(var i = 0; i < movies.length; i++){
            var mov = movies[i];
            if(cslug == mov.currentSlug) {
                return mov.children
            }
        }
    },

    bbFocus: function(e){
        var arrows = this.$('.sub-footer');
        var ct = this.$(e.currentTarget);

        if(ct.hasClass('back-button')){
            console.log('MovieScreen.bbFocus', 'Back Button Focus');
            arrows.hide();
        } else {
            console.log('MovieScreen.bbFocus', 'Folder Focus');
            arrows.show();
        }
    }
    // set "assets" model attribute, this will trigger ContentMenu to update asset list
    // this.getWidget('library').model.set('assets', children);
});

export default MovieScreen;
