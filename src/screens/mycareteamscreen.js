import Pelican from 'pelican';
import Upserver from 'upserver-client';
import CareTeamMemberList from '../widgets/careteammemberlist/controller';

require('../css/secondary.css');
require('../css/my-careteam.css');
var template = require('../templates/mycareteam.hbs');

const MyCareTeamScreen = Pelican.Screen.extend({

    className: 'secondary',
    careTeamMenuSlug: 'my-health-care-team',

    template: template,

    regions: {
        'title': {
            selector: '.page-title'
        }
    },

    keyEvents: {},

    events: {
        'click .back-button': 'back',
        'focus .menu-tab-button': 'menuFocused'
    },

    widgets: {
        menu: {
            widgetClass: Pelican.TabMenu,
            selector: '#menu'
        },
        // careteamlist
        careteamlist: {
            widgetClass: CareTeamMemberList,
            selector: '#careteamlist',
            options: {hidden: true}
        }
    },

    onKey: function(e, key){
        var handled = false;
        var careTeamKeys = ['RIGHT', 'LEFT'];
        var isKey = $.inArray(key, careTeamKeys);
        var curSlug = this.$("#menu .major .selected").attr('data-current-slug');
        var isCareTeam = (curSlug == 'my-health-care-team') ? true : false;

        console.log('MyCareTeamScreen.onKey Current Slug: ', curSlug);
        console.log('MyCareTeamScreen.onKey isKey: ', isKey);
        console.log('MyCareTeamScreen.onKey isCareTeam: ', isCareTeam);

        var wgtCareTeamList = this.getWidget('careteamlist');

        if(isCareTeam && isKey > -1) {
            console.log('MyCareTeamScreen.onKey: Sending key to care team list');
            handled = wgtCareTeamList.onKey(e, key);
        } else {
            MyCareTeamScreen.__super__.onKey.apply(this, arguments);
        }
        
        return handled;
    },

    onInit: function (options) {
        var slug = this.path.replace('home/', App.upserver.baseSlug);
        var gslug = slug.substring( 0, slug.lastIndexOf( "/" ));
        var menu = this.getWidget('menu');

        console.log('Slug: ' + slug);

        var self = this;

        // get self content
        var parent = new Pelican.Models.Content();
        parent.fetch({
            data: {
                slug: slug
            }
        }).done(function () {
            var pcont = parent.toJSON();
            self.$('.page-title').first().html(pcont.content.metadatas.title);
            if(pcont.content.metadatas.htmlHeading2)
                self.$('#heading2 p').first().html(self.decodeHTML(pcont.content.metadatas.htmlHeading2));
            menu.model.set({parent: pcont.content});

            var contents = new Pelican.Collections.ContentCollection();
            contents.fetch({
                data: {
                    slug: slug,
                    depth: 2
                }
            }).done(function () {
                console.log('APP ROUTE SEGMENT: ' + slug);
                var scont = contents.toJSON();
                var model = {menu: scont};
                self.getCareTeam();
                self.getWidget('menu').model.set(model);
                self.click('#menu1');

                menu.$('.menu-tab-button').on('focusin', function(){
                    var curslug = $(this).attr('data-current-slug');
                    if(curslug == self.careTeamMenuSlug) {
                        var wgtCareTeamList = self.getWidget('careteamlist');
                        wgtCareTeamList.focus();
                    }
                    console.log('MyCareTeamScreen.onInit We have focused on the Care Team Tab');
                });

                menu.$('.menu-tab-button').on('focusout', function(){
                    var curslug = $(this).attr('data-current-slug');
                    var isSelected = $(this).hasClass('selected');
                    if(curslug == self.careTeamMenuSlug && !isSelected) {
                        var wgtCareTeamList = self.getWidget('careteamlist');
                        wgtCareTeamList.blur();
                    }
                    console.log('MyCareTeamScreen.onInit We have blurred the Care Team Tab');
                });

            });
        });


    },

    updateCareTeam: function(data){
        var doctors = this.$('.page1 #doctors ul');
        var others = this.$('.page1 #others ul');
        
        var wgtCareTeamList = this.getWidget('careteamlist');
        wgtCareTeamList.chunkMemberList(data);

        wgtCareTeamList.$('li.memberlist:first-child').addClass('active');
    },

    getCareTeam: function(){
        var self = this;
        var defer = $.Deferred();
        upserver.api('/me/clinicalrecords?type=careteam&sort=id&maxSize=100', 'GET')
            .done(function (data) {
                self.updateCareTeam(data);
            })
            .fail(function (f) {
            });
    },

    menuFocused: function (e) {
        var $item = $(e.target);
        var path = $item.attr('data-slug').replace(App.upserver.baseSlug, 'home/');
        var id = $item.attr('data-current-slug').replace(/[\.:#]/g, '-');;
        if(path) {
            App.tracker.pageView(path, id);
        }
    }
});

export default MyCareTeamScreen ;