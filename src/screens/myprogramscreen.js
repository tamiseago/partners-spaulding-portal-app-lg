import Pelican from 'pelican';
require('../css/common.css');
require('../css/edlibrary.css');
var template = require('../templates/myprogramscreen.hbs');

const MyProgramScreen = Pelican.Screen.extend({

    className: 'myprogram edtitles',

    template: template,

    keyEvents: {},

    events: {
        'focus .back-button': 'focusBackButton',
        'focus a.ellipsis' : 'focusAsset',
        'click .back-button': 'back'
    },

    widgets: {
        bookmarks: {
            widgetClass: Pelican.ContentButtonGroup,
            selector: '#bookmarks',
            options: {nonActive: true, backButton: true}
        }
    },

    onInit: function (options) {
        var slug = this.path.replace('home/', App.upserver.baseSlug);
        console.log('Slug: ' + this.path);
        var gslug = slug.substring( 0, slug.lastIndexOf( "/" ));
        var self = this;

        var parent = new Pelican.Models.Content();
        parent.fetch({
            data: {
                slug: slug
            }
        }).done(function () {
            var pcont = parent.toJSON();
            self.$('.page-title').first().html(pcont.content.metadatas.title);
        });

        var bookmarks = new Pelican.Collections.BookmarkCollection();
        var presc = [];
        var nopresc = [];
        bookmarks.fetch({ data: { type: 'education'} })
            .done(function () {
                // some bookmarks might have invalid content (maybe no providerAssetId found, not content not uploaded yet)
                // we need to filter these bookmarks out

                var bmark = $.grep(bookmarks.toJSON(), function(b, i){
                    return (b.content);
                });

                $.each(bmark, function(i, bkm){
                    if (bkm.orderID) {
                        presc.push(bkm)
                    } else {
                        nopresc.push(bkm)
                    }
                });

                bmark = [...presc, ...nopresc];

                var bmod = {buttons: bmark};
                self.getWidget('bookmarks').model.set(bmod);
                self.bookmarkLength =bookmarks.length;
                if(bookmarks.length>=1) {
                    self.focus(self.$('.ellipsis').first());
                } else {
                    self.getWidget('bookmarks').selectFirst();
                    self.focusBackButton();
                    /*
                    this.$('.minor .menu-tab').show();
                    */
                }
            });

    },

    focusAsset: function (e) {
        this.$('.minor .menu-tab').show();
        this.$('.minor .back-tab').hide();
    },

    focusBackButton: function (e) {
        if(this.$('.ellipsis').length>=1) {
            this.$('.minor .menu-tab').hide();
            this.$('.minor .back-tab').hide();
        } else {
            this.$('.minor .menu-tab').hide();
            this.$('.minor .back-tab').show();
        }

    },

    onAttach: function () {
        this.getWidget('bookmarks').selectFirst();
    }
});

export default MyProgramScreen;