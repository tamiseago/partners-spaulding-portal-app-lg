import VideoPlayerScreen from './videoplayerscreen';
import LanguageDialog from '../widgets/languagedialog/controller';

require('../css/dialog.css');
require('../css/dialog-popup.css');
require('../css/dialog-engineering.css');

const template = require('../templates/educationvideoplayerscreen.hbs');

const EducationVideoPlayerScreen = VideoPlayerScreen.extend({

    autoStart: false,
    bookmarking: true,
    allowFastforward: false,
    allowRewind: true,
    analytics: true,

    template: template,

    creditLength: 10000,

    type: 'education',

    renditions: [],

    widgets: {
        languageDialog: {
            widgetClass: LanguageDialog,
            selector: '#languageDialog',
            options: {hidden: true}
        }
    },

    onInit: function (options) {
        var self = this;

        $('body').addClass('video');

        console.log('EducationVideoPlayerScreen.onInit: THESE ARE THE OPTIONS');
        console.log(options);

        // the 3 lines below should always be run
        self._setParams();
        self._initializeMediaEvents();

        self.getVideo()
            .done(function () {
                if (self.video) {
                    self.bookmarkIds = self.video.bookmarkIds;
                    self.duration = self.video.metadatas.runtime * 1000 || 0;
                }

                if (self.video && self.video.renditions) {
                    self.renditions = self.video.renditions;
                }
                self.chooseRendition();
            });
    },

    /**
     * !! IMPORTANT !!
     * We have to wire up events here because if a widget is hidden,
     * the initial hide() method is called with Pelican.Widget.attach().
     * By wiring up the onHide events here, we make sure that the 
     * innitial hide() is called, as these handlers require it.
     */
    onShow: function(){
        //TODO: if video has no renditions, do not show language choice
        var wgtLang = this.getWidget('languageDialog');
        wgtLang.onHide = this.hideLanguageDialog.bind(this);
    },

    chooseRendition: function () {
        var self = this;
        if (!self.renditions || self.renditions.length < 1) {
            console.log('EducationVideoPlayerScreen.chooseRendition: ERROR: no rendition to play!');
            return;
        }

        if (self.renditions.length > 1) {
            // TODO: display a dialog for language selection here
            self.showLanguageDialog.apply(self);
        }
        else {
            self.vurl = self.getDefaultUrl();
            self.startVideo();
        }
    },

    showLanguageDialog: function () {
        console.log('EducationVideoPlayerScreen.showLanguageDialog BEGIN');
        var wgtLang = this.getWidget('languageDialog');
        wgtLang.show();
        console.log('EducationVideoPlayerScreen.showLanguageDialog END');
    },

    hideLanguageDialog: function () {
        console.log('EducationVideoPlayerScreen.hidLanguageDialog BEGIN');
        var wgtLang = this.getWidget('languageDialog');

        // get questionLocale if we have more thn one language
        if (this.renditions.length >= 2) { 
            this.questionLocale = wgtLang.model.get('questionLocale');
            console.log('EducationVideoPlayerScreen.showLearnerDialog: Selected language ' + this.questionLocale);
        }
        this.vurl = this.getDefaultUrl();
        this.startVideo();
        console.log('EducationVideoPlayerScreen.hideLanguageDialog END');
    },

    getDefaultUrl: function () {
        var self = this;
        var vobj = self.video.renditions[0];
        var vurl = vobj.metadatas.url || vobj.metadatas.originalUrl;

        $.each(self.video.renditions, function (i, rendition) {
            console.log('EducationVideoPlayerScreen.getDefaultUrl rendition.type: ' + rendition.type);

            if (rendition.type == 'Original') { // this is the default url if no language matches
                if (App.config.originalUrl) {
                    vurl = rendition.metadatas.originalUrl || rendition.metadatas.url;
                } else {
                    vurl = rendition.metadatas.url || rendition.metadatas.originalUrl;
                }
            }
            var lang = (rendition.metadatas.language || 'english').toLowerCase();
            console.log(rendition.metadatas.originalUrl);
            if (lang == App.data.epicResults.renditionlang) {
                if (App.config.originalUrl) {
                    vurl = rendition.metadatas.originalUrl || rendition.metadatas.url;
                } else {
                    vurl = rendition.metadatas.url || rendition.metadatas.originalUrl;
                }
            }
        });

        return vurl;
    },

    onPlayStart: function () {
        this.updateBookmark('start', this.startPosition);
        console.log('EducationVideoPlayerScreen.onPlayStart. We have just started playing and added a bookmark\nStart Position: ' + this.startPosition);
        this._startBookmarking(20000, 'ping');
    },

    onPlayEnd: function () {
        if (!this.duration) {
            // if Razuna doesn't have video length, when we reaches end, we assume this is the length
            this.duration = this.position + 1000;
        }
    },

    onStop: function () {
        this.updateBookmark('stop');
        console.log('EducationVideoPlayerScreen.onStop. Position: ' + this.position);
        this.back();
    },

    updateBookmark: function (action) {
        var self = this;
        var defer = jQuery.Deferred();
        var q = [];

        var action = {
            action: action,
            position: Math.floor(self.position / 1000),
            duration: Math.floor(self.duration / 1000),
            param: ''
        };

        console.log('EducationVideoPlayerScreen.updateBookmark: Position = ' + action.position);
        console.log('EducationVideoPlayerScreen.updateBookmark: Duration = ' + action.duration);

        console.log('EducationVideoPlayerScreen.updateBookmark: Bookmark IDs');
        console.log(self.bookmarkIds);

        // for every bookmark with this video, update them all
        var promise = null;
        var bids = self.bookmarkIds;
        var bidLength = bids.length;
        if(bidLength > 0) {
            $.each(bids, function (i, bid) {
                q[i] = upserver.api('/me/bookmarks/' + bid + '/action', 'POST', action);
                console.log('EducationVideoPlayerScreen.updateBookmark: We have updated bookmark (' + bid + ') with action: ' + action.action);

                if( (i + 1) >= self.bookmarkIds.length) {
                    q[i].done(function(){
                        console.log('EducationVideoPlayerScreen.updateBookmark: We\'ve iterated over all bookmarks');
                        defer.resolve();
                    });
                }
            });
        } else {
            upserver.api('/me/bookmarks/' + self.bookmarkId + '/action', 'POST', action)
                .done(function(){
                    console.log('EducationVideoPlayerScreen.updateBookmark (SINGLETON): We have updated bookmark (' + self.bookmarkId + ') with action: ' + action.action);
                    defer.resolve();
                });
        }

        return defer.promise();
    }

});

export default EducationVideoPlayerScreen;