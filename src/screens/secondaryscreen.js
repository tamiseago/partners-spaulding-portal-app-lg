import Pelican from 'pelican';

require('../css/secondary.css');
var template = require('../templates/secondaryscreen.hbs');

const SecondaryScreen = Pelican.Screen.extend({

    className: 'secondary',

    template: template,

    regions: {
        'title': {
            selector: '.page-title'
        }
    },

    keyEvents: {},

    events: {
        'click .back-button': 'back',
        'focus .menu-tab-button': 'menuFocused'
    },

    widgets: {
        menu: {
            widgetClass: Pelican.TabMenu,
            selector: '#menu'
        }
    },

    onInit: function (options) {
        var slug = this.path.replace('home/', App.upserver.baseSlug);
        var gslug = slug.substring( 0, slug.lastIndexOf( "/" ));

        console.log('Slug: ' + slug);
        var self = this;

        var contents = new Pelican.Collections.ContentCollection();
            contents.fetch({
                data: {
                    slug: slug,
                    depth: 2
                }
            }).done(function () {
                // get self content
                var parent = new Pelican.Models.Content();
                parent.fetch({
                    data: {
                        slug: slug
                    }
                }).done(function () {
                    var pcont = parent.toJSON();
                    self.$('.page-title').first().html(pcont.content.metadatas.title);
                    if(pcont.content.metadatas.htmlHeading1)
                        self.$('#heading1').first().html(self.decodeHTML(pcont.content.metadatas.htmlHeading1));
                    if(pcont.content.metadatas.htmlHeading2)
                        self.$('#heading2 p').first().html(self.decodeHTML(pcont.content.metadatas.htmlHeading2));
                    self.getWidget('menu').model.set({parent: pcont.content});

                    console.log('APP ROUTE SEGMENT: ' + slug);
                    var scont = contents.toJSON();
                    var model = {menu: scont};
                    self.getWidget('menu').model.set(model);
                    self.getWidget('menu').focus('#menu1');
                    self.getWidget('menu').click('#menu1');
                    self.updateLevel();
                });
            });
    },

    onAttach: function () {

    },

    updateLevel: function () {
        if (App.data && App.data.device && App.data.device.deviceProvision && App.data.device.deviceProvision.location) {
            var location = App.data.device.deviceProvision.location;
            var room = location.room;
            var floor = location.floor;
            var category = location.categoryName;
            var level = ' ' + floor + ': ' + category + ' Room ' + room;
            this.$('.level').text(level);
        }
    },

    decodeHTML : function (html) {
        var txt = document.createElement('textarea');
        txt.innerHTML = html;
        return txt.value;
    },

    menuFocused: function (e) {
        var $item = $(e.target);
        var path = $item.attr('data-slug').replace(App.upserver.baseSlug, 'home/');
        var id = $item.attr('data-current-slug').replace(/[\.:#]/g, '-');;
        if(path) {
            App.tracker.pageView(path, id);
        }
    }
});

export default SecondaryScreen;
