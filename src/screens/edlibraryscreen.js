import Pelican from 'pelican';
import ContentMenuCheckList from '../widgets/contentmenuchecklist/controller';

require('../css/edlibrary.css');
var template = require('../templates/edlibraryscreen.hbs');

const EdLibraryScreen = Pelican.Screen.extend({

    className: 'edlibrary edtitles',

    template: template,
    xhrContent: {},

    keyEvents: {},

    events: {
        'focus .back-button': 'focusBackButton',
        'focus .folder': 'focusFolder',
        'blur .folder': 'blurFolder',
        'click .back-button': 'back',
        'click .folder': 'selectFolder'
    },

    widgets: {
        library: {
            widgetClass: ContentMenuCheckList,
            selector: '#menu',
            options: {}
        }
    },

    onInit: function (options) {
        var slug = this.path.replace('home/', App.upserver.baseSlug);
        console.log('EDLibraryScreen.onInit.path: ' + this.path);
        console.log('EDLibraryScreen.onInit.slug: ' + slug);

        var self = this;

        // get self content
        var parent = new Pelican.Models.Content();
        parent.fetch({
            data: {
                slug: slug
            }
        }).done(function () {
            self.$('.page-title').first().html(parent.toJSON().content.metadatas.title);
        });

        var folders = new Pelican.Collections.ContentCollection();
        folders.fetch({
            data: {
                slug: slug,
                depth: 1
            }
        })
            .done(function () {
                var content = folders.toJSON();
                self.getWidget('library').model.set('folders', content);
                self.getWidget('library').selectFirstFolder();
            });
    },

    onAttach: function () {
        this.getWidget('library').selectFirstFolder();
    },

    focusFolder: function (e) {
        this.$('#heading2').show();
    },

    focusBackButton: function (e) {
        this.$('#heading2 p').hide();
        this.$('.minor .content-button-group').html('');
        this.getWidget('library').model.set('assets', {});
    },

    selectFolder: function (e) {
        var children = null;
        var $obj = $(e.target);
        var slug = $obj.attr('data-slug');

        var self = this;

        self.xhrContent = {};
        var assets = new Pelican.Collections.ContentCollection()
        self.xhrContent = assets.fetch({
            data: {
                slug: slug,
                depth: 1
            }
        })
            .done(function () {
                var content = assets.toJSON();
                self.getWidget('library').model.set('assets', content);
            });
    },

    blurFolder: function(){
        this.xhrContent.abort();
    }
});

export default EdLibraryScreen;
