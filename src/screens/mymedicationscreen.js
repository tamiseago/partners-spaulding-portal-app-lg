import SecondaryScreen from './secondaryscreen';
import i18n from 'i18next';
import Pelican from 'pelican';
import MedicatinIntroDialog from '../widgets/medicationintrodialog/controller';
import MedicationInfo from '../widgets/medicationinfo/controller';
import MedicationPinErrorDialog from '../widgets/medicationpinerrordialog/controller';
import { appendFile } from 'fs';

// require('../css/rs-medlist.css');
// require('../css/rs-securitycode.css');

require('../css/secondary.css');
require('../css/my-medications.css');
require('../css/dialog.css');
require('../css/dialog-popup.css');
require('../css/dialog-engineering.css');
var template = require('../templates/mymedicationsscreen.hbs');

const MyMedicationScreen = SecondaryScreen.extend({

    className: 'secondary mymedications',

    template: template,

    widgets: {
        menu: {
            widgetClass: Pelican.TabMenu,
            selector: '#menu'
        },
        dialog: {
            widgetClass: MedicatinIntroDialog,
            selector: '#dialog'
        },
        medInfo: {
            widgetClass: MedicationInfo,
            selector: "#medInfo"
        },
        pinError: {
            widgetClass: MedicationPinErrorDialog,
            selector: '#pin-error'
        }
    },

    keyEvents: {
        // "ENTER": "onSelect",
        // "RIGHT": "focusMedInfo"
    },

    meds: {
        sch: {},
        prn: {}
    },

    hasPermission: false,
    hasFocus: true,

    events: function () {
        return $.extend({}, SecondaryScreen.prototype.events, {
            'click #accept': 'selectAccept',
            'click #decline': 'selectDecline',
            'click .requestpharvisit': 'sendInboxRequest',
            'click .menu-tab-button': 'menuClicked',
            'focus .menu-tab-button': 'menuFocused'
        })
    },

    initMedInfo: function(){
        var self = this;
        var def = $.Deferred();
        var lang = App.data.language;
        var strSch = 'sch_' + lang;
        var strPrn = 'prn_' + lang;

        var wmed = self.getWidget('medInfo');
        var menu = self.getWidget('menu');

        console.log('MyMedicationScreen.initMedInfo starting to retrieve data.');
        wmed.getRxInfo(strSch)
            .done(function(sdata){
                self.meds.sch.data = sdata;
                wmed.getRxInfo(strPrn)
                    .done(function(pdata){
                        self.meds.prn.data = pdata;
                        console.log('MyMedicationScreen.initMedInfo we got all of our MED data.');
                        console.log(self.meds);
                        def.resolve();
                    });
            });

        wmed.model.on('change:hasFocus', function(model){
            if(model.get('hasFocus') == false) {
                self.hasFocus = true;
                $('#medinfo-overlay').remove();
                console.log('MyMedicationScreen.initMedInfo: MODEL CHANGED');
                console.log(model);
            } else {
                if($("#medinfo-overlay").length == 0) {
                    self.$el.prepend('<div id="medinfo-overlay" />');
                }
            }
        });

        return def;
        
    },

    onKey: function(e, key){
        var self = this;
        var handled = false;
        var pin = this.getWidget('dialog');
        var menu = this.getWidget('menu');
        var med = this.getWidget('medInfo');

        if(this.hasFocus == false) {

            this.getActiveWidget().done(function(wkey){
                var widget = self.getWidget(wkey);
                widget.onKey(e, key);
            });
            return true;
        }
        
        switch(key) {
            case "RIGHT":
            case "ENTER":
            case "SELECT":
                if(!self.isMedInfo()) {
                    if(key != "RIGHT") {
                        self.sendInboxRequest(e);
                    }
                    break;
                }

                med.model.set('hasFocus', true);
                this.hasFocus = false;
                if(menu.$("#back").hasClass("selected")) {
                    menu.$("#back").trigger('click');
                } else {
                    med.focus("#medlist li:first-child");
                }
                handled = true;
                break;
            case "LEFT":
                med.hasFocus = false;
                this.hasFocus = true;
                handled = true;
                break;
            case "ESCAPE":
            case "EXIT":
                self.focusScreen(e);
                handled = true;
                break;
            default:
                self.blur(med.$el);
                self.focus(menu.$el);
                if(this.hasFocus == true) {
                    handled = MyMedicationScreen.__super__.onKey.apply(self, arguments);
                }

                break;
        }

        return handled;
    },

    isMedInfo: function(){
        var menu = this.getWidget('menu');
        var mid = menu.$('.selected').attr('id');

        console.log("MyMedicationScreen.isMedInfo MenuID: " + mid);

        return (mid == "menu3") ? false : true;
    },

    focusScreen: function(e){
        var med = this.getWidget('medInfo');
        var menu = this.getWidget('menu');
        this.blur(med.$el);
        this.focus(menu.$el);
        console.log("MyMedicationScreen.focusScreen");
    },

    getActiveWidget: function(){
        var self = this;
        var def = $.Deferred();

        $.each(self.widgets, function(key, val){
            console.log("MyMedicationScreen.getActiveWidget -- key: " + key + ": " + val);
            var widget = self.getWidget(key);
            if(widget.model.get('hasFocus')) {
                console.log("MyMedicationScreen.getActiveWidget -- ACTIVE: " + key);
                def.resolve(key);
            }
        });

        return def;
    },

    onInit: function (options) {
        var slug = this.path.replace('home/', App.upserver.baseSlug);
        console.log('MedicationScreen Slug: ' + slug);

        var self = this;

        var type = self.params[0];
        console.log('MedicationScreen.type ' + type);

        var contents = new Pelican.Collections.ContentCollection();
        contents.fetch({
            data: {
                slug: slug,
                depth: 2
            }
        }).done(function () {
            console.log('APP ROUTE SEGMENT: ' + slug);
            var content = contents.toJSON();
            var model = {menu: content};
            var lang = (App.data.language || 'english').toLowerCase();
            var menu = self.getWidget('menu');

            console.log('MyMedicationScreen.onInit.content ' + content);

            self.meds.prn.data
            for(var i = 0; i < content.length; i++) {
                var cont = content[i];
                var type = cont.currentSlug;
                var meds = self.meds[type];
                if(meds) {
                    var meta = cont.content.metadatas;
                    self.meds[type].screenTitle = meta.title;
                    self.meds[type].screenDesc = meta.description;
                }
            }

            menu.model.set(model);
            // self.getWidget('menu').click('#menu1');
            if(self.hasPermission) {
                self.initMedInfo()
                .done(function(){
                    dialog.hide();
                    self.focus(menu.$el);
                    self.hasPermission = true;
                    widget.model.set(self.meds[type]);
                    menu.focus("#menu1");
                    console.log('MyMedicationScreen.onInit Medical Information Initialized');
                });
            } else {
                var pinError = self.getWidget('pinError');
                self.initPin();

                pinError.onClose = function(e){
                    console.log('MyMedicationScreen.onInit: We are trying to close the Pin Error Dialog');
                    self.back();
                }
            }
        });
    },

    onSelect: function(e){
        console.log("MyMedicationScreen.onSelect");
        this.getWidget('dialog').hide();
    },

    onDestroy: function(){
        this.hasPermission = false;
    },

    back: function(){
        this.hasPermission = false;
        MyMedicationScreen.__super__.back.apply(this, arguments);
    },

    initPin: function(){
        var self = this;
        var pin = self.getWidget('dialog');
        var pinError = self.getWidget('pinError');
        pin.onHide = function(){
            if(!self.hasPermission) {
                self.hasFocus = false;
                pinError.show();
                pinError.model.set('hasFocus', true);
            } else {
                self.hasFocus = true;
            }
        };
        self.hasFocus = false;
        pin.show();
        pin.model.set('hasFocus', true);
    },

    checkPin: function(val){
        var mrn = App.data.patient.mrn;
        var mrnLastFour = mrn.substr(mrn.length - 4);

        return (val == mrnLastFour);
    },

    showAcceptance: function () {
        var self = this;
        self.getWidget('dialog').show();
        self.focus('#accept');
    },

    selectAccept: function () {
        var self = this;
        var type = self.params[0];
        var widget = self.getWidget('medInfo');
        var dialog = self.getWidget('dialog');
        var pinInput = dialog.getWidget('pinInput');
        var pin = pinInput.model.get('pin');
        var menu = self.getWidget('menu');

        if(this.checkPin(pin)) {
            console.log('MyMedicationScreen.selectAccept: PIN is correct');
            self.hasPermission = true;
            self.initMedInfo()
                .done(function(){
                    dialog.hide();
                    self.focus(menu.$el);
                    widget.model.set(self.meds[type]);
                    menu.focus("#menu1");
                    console.log('MyMedicationScreen.selectAccept: Medical Information Initialized');
                });
        } else {
            console.log('MyMedicationScreen.selectAccept: PIN is wrong');
            pinInput.resetPin();
            dialog.hide();
        }

        
    },

    selectDecline: function () {
        this.hasPermission = false;
        this.back()
    },

    showPharNotification: function () {
        var self = this;
        self.$('.minor').hide();
        self.$('#pharvisit-notification').show();

        $.doTimeout(5000, function () {
            self.$('#pharvisit-notification').hide();
            self.$('.minor').show();
        });
    },

    showDisMedsNotification: function () {
        var self = this;
        self.$('#dischargemeds-notification').show();

        $.doTimeout(5000, function () {
            self.$('#dischargemeds-notification').hide();
        });

    },

    sendInboxRequest: function (e) {
        var mrn = App.data.patient.mrn;
        var fullname = App.data.patient.patientProfile.fullName;
        var acct = App.data.patient.patientVisit.accountNumber;
        var dob = App.data.patient.patientProfile.dateOfBirth;
        var location = App.data.device.deviceProvision.location;
        var room = location.room;
        var url = App.config.mirth.host + "/?mrn=" + mrn + "&fullname=" + fullname + "&acct=" + acct + "&room=" + room + "&dob=" + dob + "&type=requestpharvisit" ;
        App.upserver.api('/proxy', 'GET', {
            "url": url
        });
        console.log('message sent ' + url);
        App.tracker.event('medication', 'requestpharvisit');
        this.showPharNotification();
    },

    menuFocused: function (e) {
        var self = this;
        var target = $(e.target);
        var menu = self.getWidget('menu');
        var medinf = self.getWidget('medInfo');

        if(menu.$("#menu3").hasClass("selected")) {
            self.$("#medInfoContainer").hide();
        } else {
            var type = target.attr('data-current-slug');
            var medinfo = self.getWidget('medInfo');
            medinfo.model.set('screenTitle', self.meds[type].screenTitle);
            medinfo.model.set('screenDesc', self.meds[type].screenDesc);
            medinfo.model.set('medications', self.meds[type].data);

            self.$("#medInfoContainer").show();

            console.log('MyMedicationScreen.menuFocused.mid' +
                '\ntype: ' + type +
                '\nmeds[type]: ' + self.meds[type]);
        }
        MyMedicationScreen.__super__.menuFocused.apply(self, arguments);
    },

    menuClicked: function(e){
        e.preventDefault();
        return false;
    }

});

export default MyMedicationScreen;