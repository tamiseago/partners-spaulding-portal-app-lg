import Pelican from 'pelican';
import ApiHelper from '../helpers/apihelper';

require('../css/global.css');
require('../css/common.css');
require('../css/intro2.css');

var template = require('../templates/intro2screen.hbs');

const Intro2Screen = Pelican.Screen.extend({

    className: 'intro2',

    template: template,

    keyEvents: {
        'POWER': true,
        'EXIT': true,
        'BACK': true,
        'HOME': true,
        'CLOSE': true
    },

    events: {
        'click #accept': 'selectAccept',
        'click #decline': 'selectDecline'
    },

    widgets: {
        buttons: {
            widgetClass: Pelican.ButtonGroup,
            selector: '#buttons',
            options: { orientation: 'vertical', preRender: true }
        }
    },

    toggleMute: function() {

    },

    selectAccept: function(e) {
        e.preventDefault();
        console.log('select accept');
        this.saveTerm('accept');
        this.saveLanguage('en');
    },

    selectDecline: function(e) {
        e.preventDefault();
        console.log('select decline');
        this.saveTerm('decline');
        App.router.go('discharged/tv');
    },

    saveTerm: function(term) {
        // term is visit level preference and should reset every time patient discharges
        ApiHelper.setPreference('term', term);
    },

    saveLanguage: function(lang) {
        if(!lang)
            return;

        // update language.
        App.data.language = lang;   // global option
        window.upserver.setLanguage(lang);  // set client API cookie
        window.upserver.locale = lang;  // set client API language header
        ApiHelper.setPreference('language', lang); // save to server

        // load new resource
        i18next.changeLanguage(lang, function() {
            App.layout.closeAllScreens();
            App.router.go('home');
        });

        // analytics
        App.tracker.event('language', 'set', lang);
    }
});

export default Intro2Screen;