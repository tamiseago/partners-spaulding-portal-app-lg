import Pelican from 'pelican';
import SecondaryScreen from './secondaryscreen';
import VideoSeriesMenu from '../widgets/videoseriesmenu/controller';
require('../css/videoseries.css');

var tpl = require('../templates/videoseriesepisodesscreen.hbs');

const VideoSeriesEpisodesScreen = SecondaryScreen.extend({
    template: tpl,
    className: 'secondary videoSeriesEpisodes',

    events: {
        'focus .back-button': 'focusBackButton',
        'focus .menu-tab-button-folder' : 'focusButton',
        'click .back-button': 'back'
    },

    widgets: {
        menu: {
            widgetClass: VideoSeriesMenu,
            selector: '#menu'
        }
    },

    onInit: function (options) {
        var slug = this.path.replace('home/', App.upserver.baseSlug);
        var gslug = slug.substring( 0, slug.lastIndexOf( "/" ));


        console.log('VideoSeriesEpisodesScreen.onInit.slug: ' + slug);


        var self = this;
        var menu = self.getWidget('menu');

        // get self content
        var parent = new Pelican.Collections.ContentCollection();
        parent.fetch({
            data: {
                slug: slug
            }
        }).done(function () {
            var pcont = parent.toJSON();
            menu.model.set('menu', pcont);
            // self.$("#menu1").focus();

            console.log('VideoSeriesEpisodesScreen.onInit.pcontent:');
            console.log(pcont);

            // get self parent content
            var gparent = new Pelican.Models.Content();
            gparent.fetch({
                data: {
                    slug: slug
                }
            }).done(function () {
                var gpcont = gparent.toJSON();
                menu.model.set({seriesName: gpcont.content.name});

                self.getWidget('menu').focus('#menu1');
                
                console.log('VideoSeriesEpisodesScreen.onInit.gpcont:');
                console.log(gpcont);
            });
        });
    },

    onScreenShow: function(){
    },

    onRender: function(){
        var posters = this.getWidget('menu');
        posters.contentUrlPrefix = 'series';
    },

    focusButton: function (e) {
        this.$('.minor').show();
    },

    focusBackButton: function (e) {
        this.$('.minor').hide();
    }



});

export default VideoSeriesEpisodesScreen;
