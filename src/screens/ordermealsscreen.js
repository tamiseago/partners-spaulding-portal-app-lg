import Pelican from 'pelican';
import Upserver from 'upserver-client';

require('../css/secondary.css');
require('../css/order-meals.css');
var template = require('../templates/ordermealsscreen.hbs');

const OrderMealsScreen = Pelican.Screen.extend({

    className: 'secondary ordermeals',

    template: template,

    regions: {
        'title': {
            selector: '.page-title'
        }
    },

    keyEvents: {},

    events: {
        'click .back-button': 'back',
        'focus .menu-tab-button': 'menuFocused'
    },

    widgets: {
        menu: {
            widgetClass: Pelican.TabMenu,
            selector: '#menu'
        }
    },

    onInit: function (options) {
        var slug = this.path.replace('home/', App.upserver.baseSlug);
        console.log('Slug: ' + this.path);

        var self = this;

        var contents = new Pelican.Collections.ContentCollection();
        contents.fetch({
            data: {
                slug: slug,
                depth: 2
            }
        })
            .done(function () {
                console.log('APP ROUTE SEGMENT: ' + slug);
                var content = contents.toJSON();
                var model = {menu: content};
                var diet = '';
                var dietvalue = '';
                self.$('#menu2-tab .sub-content').html(diet);

                Upserver.api('/me/clinicalrecords?type=diet', 'GET')
                    .done(function (data) {
                        dietvalue = JSON.parse(data[0].value);
                        console.log(dietvalue)
                        if (data && data.length >= 0) {
                            diet = '<br><br><br><br><span class="text-color-5">' + dietvalue.name + '</span><br><br>'
                            diet = diet + dietvalue.description;
                            console.log('diet ' + diet);
                            self.$('#menu2-tab .sub-content').html(diet);
                        }
                    });

                self.getWidget('menu').model.set(model);
                self.getWidget('menu').click('#menu1');
            });

    },

    onAttach: function () {
    },

    menuFocused: function (e) {
        var $item = $(e.target);
        var path = $item.attr('data-slug').replace(App.upserver.baseSlug, 'home/');
        var id = $item.attr('data-current-slug').replace(/[\.:#]/g, '-');;
        if(path) {
            App.tracker.pageView(path, id);
        }
    }
});

export default OrderMealsScreen;