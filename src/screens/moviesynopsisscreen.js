import Pelican from 'pelican';
require('../css/common.css');
require('../css/movies-synopsis.css');
var template = require('../templates/moviesynopsisscreen.hbs');

const MovieSynopsisScreen = Pelican.Screen.extend({

    className: 'moviesynopsis',

    video: new Pelican.Models.Content(),

    template: template,

    creditLength: 1,

    keyEvents: {},

    events: {
        'click .back-button': 'back'
    },

    widgets: {

        buttons: {
            widgetClass: Pelican.ButtonGroup,
            selector: '#buttons',
            options: {orientation: 'vertical', preRender: true}
        }

    },

    calcDuration: function (duration) {
        if(!duration)
            return '00:00';
        var str = '';
        if(duration>=3600) {
            var h = Math.floor(duration/3600);
            str += h + ':';
            duration = duration%3600;
        }
        if(duration>=60) {
            var mm = Math.floor(duration/60);
            str += (mm<10?'0':'') + mm + ':';
            duration = duration%60;
        }
        var ss = duration;
        str += (ss<10?'0':'') + ss;
        return str;
    },

    getVideo: function(videoId){
        var self = this;
        var defer = jQuery.Deferred();

        var vid = videoId || self.vid;

        self.video.fetch({
            uri: '/contents',
            data:{
                id: vid
            }
        })
            .done(function(){
                self.state = 'loaded';
                console.log(self.video);
                defer.resolve();
            })
            .fail(function(f){
                defer.reject(f.errorMessage);
            });

        return defer.promise();
    },


    onInit: function () {
        var self = this;
        console.log('video queries: ' + JSON.stringify(self.queries));
        $('body').addClass('video');
        if (self.params.length > 1) {
            self.vid = self.params[0];
        }

        if (self.queries && self.queries['vid']) {
            self.vid = self.queries['vid'];
        }

        console.log('vid = ' + self.vid);

    },


    onScreenShow: function () {

        var self = this;
        self.lastPosition = 0;
        self.$('.button-group').hide();
        self.$('.minor .sub-text1').hide();
        self.$('#heading1').hide();
        self.getVideo()
            .done(function () {

                self.blur('a');
                self.focus('#restart');

                if (self.video && self.video.attributes) {
                    console.log('!-- We are done getting Movie Data --!');
                    self.bookmarkIds = self.video.get('bookmarkIds') || [];
                    self.bookmarkId = self.video.get('bookmarkIds')[0];
                    self.title = self.video.attributes.metadatas.title || self.video.get('metadatas').title || self.video.attributes.name || '';
                    self.runTime = self.calcDuration(self.video.attributes.metadatas.runtime);
                    self.rating = self.video.attributes.metadatas.rating || "NR";
                    self.description = self.video.attributes.metadatas.description;
                    self.thumbnail = self.video.get('thumbnail');
                }
                var rating = ''
                if(self.rating)
                    rating = 'Rated ' + self.rating;
                self.$('#heading1 img').attr('src', self.thumbnail);
                self.$('.movie-title').html(self.title);
                self.$('.movie-rating').html(rating);
                self.$('.movie-run-time').html(self.runTime);
                self.$('.movie-synopsis').html(self.description);
                self.$('.movie-instruction').show();
                self.$('#restart').attr('href', '#movieplayer/' + self.params[0]);

                if(self.bookmarkIds.length > 0) {
                    $('#resume').show();
                    var bookmark = new Pelican.Models.Bookmark({id: self.bookmarkId});
                    self.lastPosition = 0;
                    bookmark.fetch()
                        .done(function () {
                            console.log(bookmark);
                            self.lastPosition = bookmark.attributes.lastPosition;
                            var lpWithCredits = self.creditLength + self.lastPosition;
                            var rt = bookmark.attributes.content.metadatas.runtime;

                            self.duration = self.calcDuration(self.runTime);
                            self.$('#heading1 .text-large').html(self.title);
                            self.$('#resume').attr('href', '#movieplayer/' + self.params[0] + '?start=' + self.lastPosition* 1000 );
                            
                            self.blur('#restart');
                            self.focus('#resume');

                            /**
                             * This was a quick hack to handle the show/hide of
                             * particualar buttons. This shoudl be cleaned up. {GS}
                             */
                            var status = "NotStarted";
                            if(bookmark && self.lastPosition > 0) {
                                if(lpWithCredits >= rt){
                                    status = "Completed";
                                } else if(lpWithCredits < rt) {
                                    status = "InProgress";
                                } else {
                                    status = self.bookmark.attributes.status;
                                }
                            }
                            
                            console.log('MovieSynopsisScreen.onScreenShow: lastPosition ' + self.lastPosition);
                            console.log('MovieSynopsisScreen.onScreenShow: lpWithCredits ' + lpWithCredits);
                            console.log('MovieSynopsisScreen.onScreenShow: bookmark...runtime ' + rt);
                            console.log('MovieSynopsisScreen.onScreenShow: duration ' + self.duration);
                            console.log('MovieSynopsisScreen.onScreenShow: Status ' + status);

                            switch(status){
                                case 'Completed':
                                    self.$('a#restart').show();
                                    self.$('a#resume').hide();
                                    self.focus($('a#restart'));
                                    break;
                                case 'NotStarted':
                                    self.$('a#restart').show();
                                    self.$('a#resume').hide();
                                    self.focus($('a#restart'));
                                    break;
                                case 'InProgress':
                                    self.blur(self.$('a#restart'));
                                    self.focus(self.$('a#resume'));
                                    break;
                                default:
                                    self.$('a#restart').show();
                                    self.$('a#resume').hide();
                                    self.focus($('a#restart'));
                                    break;
                            }
                            self.$('.button-group').show();
                            self.$('.minor .sub-text1').show();
                            self.$('#heading1').show();
                        });
                    } else {
                        self.$('.button-group').show();
                        self.$('.minor .sub-text1').show();
                        self.$('#heading1').show();
                }
            });
    },

    onAttach: function () {
    }
});

export default MovieSynopsisScreen;
