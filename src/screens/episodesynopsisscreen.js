import Pelican from 'pelican';
import MovieSynopsisScreen from './moviesynopsisscreen';
require('../css/movies-synopsis.css');
require('../css/episode-synopsis.css');
var template = require('../templates/episodesynopsisscreen.hbs');

const EpisodeSynopsisScreen = MovieSynopsisScreen.extend({

    className: 'moviesynopsis episodesynopsis',
    template: template,

    keyEvents: {},

    events: {
        'focus .back-button': 'focusBackButton',
        'focus .menu-tab-button' : 'focusButton',
        'click .back-button': 'back'
    },

    focusButton: function (e) {
        this.$('.minor .menu-tab').show();
        this.$('.minor .back-tab').hide();
    },

    focusBackButton: function (e) {
            this.$('.minor .menu-tab').hide();
    }

});

export default EpisodeSynopsisScreen;
