import Pelican from 'pelican';
import EducationVideoPlayerScreen from './educationvideoplayerscreen';
import VideoPlayerScreen from './videoplayerscreen';
import Intl from '../helpers/intl';

import LearnerDialog from '../widgets/learnerdialog/controller';
import VideoComprehensionDialog from '../widgets/videocomprehensiondialog/controller';
import FeedbackDialog from '../widgets/feedbackdialog/controller';
import LanguageDialog from '../widgets/languagedialog/controller';

require('../css/dialog.css');
require('../css/dialog-popup.css');
require('../css/dialog-engineering.css');
require('../css/interface-videoplayer.css');

var template = require('../templates/interfacevideoplayerscreen.hbs');

const InterfaceVideoPlayerScreen = EducationVideoPlayerScreen.extend({

    template: template,

    questionLocale: 'en',
    allowRewind: false,
    isWelcomeVideo: false,
    bookmarks: [],
    bookmark: {},

    widgets: {
        languageDialog: {
            widgetClass: LanguageDialog,
            selector: '#languageDialog',
            options: {hidden: true}
        },
        learnerDialog: {
            widgetClass: LearnerDialog,
            selector: '#learnerDialog',
            options: {hidden: true}
        },
        videoComprehensionDialog: {
            widgetClass: VideoComprehensionDialog,
            selector: '#videoComprehensionDialog',
            options: {hidden: true}
        },
        feedbackDialog: {
            widgetClass: FeedbackDialog,
            selector: '#feedbackDialog',
            options: {hidden: true}
        }
    },

    events: {
        'click #next': 'selectNext',
        'click #close': 'closePlayer'
    },

    onInit: function(){
        var isFirst = true;
        var self = this;

        self.isWelcomeVideo = self._isWelcomeVideo();
        InterfaceVideoPlayerScreen.__super__.onInit.apply(this, arguments);
    },

    onRender: function(){
    },

    getBookmarks: function(){
        var defer = jQuery.Deferred();

        upserver.api('/me/bookmarks')
            .done(function(bookmarks){
                console.log('InterfaceVideoPlayerScreen.getBookmarks');
                console.log(bookmarks);
                defer.resolve(bookmarks);
            });

        return defer.promise();
    },
    
    getBookmark() {
        var self = this;
        var wvid = self.vid;
        var defer = jQuery.Deferred();

        console.log('InterfaceVideoPlayerScreen.getBookmark: Video ID: ' + wvid);

        this.getBookmarks()
            .done(function(bkmrks){
                var bookmarks = self.bookmarks = bkmrks;
                if(bookmarks.length > 0) {
                    for(var i = 0; i < bookmarks.length; i++) {
                        var b = bookmarks[i];
                        if(b.contentId == wvid) {
                            console.log('InterfaceVideoPlayerScreen.getBookmark: We found a bookmark and will resume.');
                            self.bookmark = b;
                            defer.resolve();
                        }
                    }
                    console.log('InterfaceVideoPlayerScreen.getBookmark: We were not able to find a bookmark.');
                    defer.resolve();
                } else {
                    console.log('InterfaceVideoPlayerScreen.getBookmark: There are no bookmarks for this video.');
                    defer.resolve();
                }
            });

        return defer.promise();
    },

    /**
     * !! IMPORTANT !!
     * We have to wire up events here because if a widget is hidden,
     * the initial hide() method is called with Pelican.Widget.attach().
     * By wiring up the onHide events here, we make sure that the 
     * innitial hide() is called, as these handlers require it.
     */
    onShow: function(){
        //TODO: if video has no renditions, do not show language choice
        var wgtLang = this.getWidget('languageDialog');
        var wgtLearner = this.getWidget('learnerDialog');
        var wgtVidComp = this.getWidget('videoComprehensionDialog');
        var wgtFeedback = this.getWidget('feedbackDialog');

        wgtLang.onHide = this.showLearnerDialog.bind(this);
        wgtLearner.onHide = this.onLeanerSelection.bind(this);
        wgtVidComp.onHide = this.onVideoComprehensionSelection.bind(this);
        wgtFeedback.onHide = this.onFeedbackSelection.bind(this);
    },

    chooseRendition: function () {

        var self = this;
        if (self.queries && self.queries['bookmarkId']) {
            self.bookmarkId = self.queries['bookmarkId'];
        }
        else if (self.bookmarkIds) { // if bookmarkId is not passed in from URL, use the one from EducationVideoPlayer
            self.bookmarkId = self.bookmarkIds[0];
        }

        console.log('InterfaceVideoPlayerScreen.chooseRendition.bookmarkId: ' + self.bookmarkId);

        self.getBookmark()
            .done(function(){
                App.data.epicResults = [];
                App.data.epicResults.renditionlang = 'english';
                App.data.epicResults.learners = [];
                App.data.epicResults.completion = 0;
                App.data.epicResults.understanding = 'NL';
                
                if (!self.renditions || self.renditions.length <= 0) {
                    console.log('InterfaceVideoPlayerScreen.chooseRendition ERROR: no rendition to play!');
                    return;
                }
                console.log('self renditions');
                console.log(self.renditions);
                if (self.renditions.length >= 2) {
                    console.log('InterfaceVideoPlayerScreen.chooseRendition More than 1 rendition');
                    self.showLanguageDialog.apply(self);
                } else {
                    console.log('InterfaceVideoPlayerScreen.chooseRendition Only 1 rendition');
                    console.log('InterfaceVideoPlayerScreen.chooseRendition redition length: ' + self.renditions.length);
                    self.showLearnerDialog.apply(self);
                }
            })

        
    },

    onPlayEnd: function () {
        if (!this.duration) {
            // if Razuna doesn't have video length, when we reaches end, we assume this is the length
            this.duration = this.position + 1000;
        }
    },

    onStop: function (destroyScreen) {
        var self = this;
        destroyScreen = (destroyScreen) ? destroyScreen : false;
        console.log('InterfaceVideoPlayerScreen.onStop, destroyScreen: ' + destroyScreen + ', position: ' + self.position + ', duration: ' + self.duration);

        self.updateBookmark('stop')
            .done(function(){
                console.log('InterfaceVideoPlayerScreen.onStop: We are going to stop bookmarking');
                self._stopBookmarking();
            });

        console.log('InterfaceVideoPlayerScreen.onStop: Bookmark has been updated to stop');

        var creditLength = self.creditLength;
        var pos = Math.floor(self.position) + creditLength;
        console.log('InterfaceVideoPlayerScreen.onStop position: ' + pos + ' \tDuration: ' + self.duration);

        if (pos > self.duration) {
            App.data.epicResults.completion = 'Complete';
            if(destroyScreen) {
                // user is closing screen, no need to display dialog
                console.log('InterfaceVideoPlayerScreen.onStop: user closed screen.');
                self.sendResults()
                    .done(function(){
                        self.back();
                    });
            }
            else {
                console.log('InterfaceVideoPlayerScreen.onStop:showing comprehension dialog');
                self.showVideoComprehensionDialog();
            }
        } else {
            App.data.epicResults.completion = 'Incomplete';
            console.log('InterfaceVideoPlayerScreen.onStop: video is not complete.');

            self.sendResults()
                .done(function(){
                    
                    if(self.isWelcomeVideo) {
                        console.log('InterfaceVideoPlayerScreen.onStop: DID NOT COMPLETE! We are on the welcome screen, so we will go back to the intro.');
                        self.back();
                    } else {
                        console.log('InterfaceVideoPlayerScreen.onStop: DID NOT COMPLETE! This IS NOT the Welcome Video.');
                        self.back();
                    }
                });
            
            
        }

        this.resultSent = true;
    },

    showLanguageDialog: function () {
        console.log('InterfaceVideoPlayerScreen.showLanguageDialog BEGIN');
        var wgtLang = this.getWidget('languageDialog');
        wgtLang.show();
        console.log('InterfaceVideoPlayerScreen.showLanguageDialog END');
    },

    showLearnerDialog: function () {
        console.log('InterfaceVideoPlayerScreen.showLearnerDialog');
        var wgtLearner = this.getWidget('learnerDialog');
        var wgtLang = this.getWidget('languageDialog');

        // get questionLocale if we have more thn one language
        if (this.renditions.length >= 2) { 
            this.questionLocale = wgtLang.model.get('questionLocale');
            console.log('InterfaceVideoPlayerScreen.showLearnerDialog: Selected language ' + this.questionLocale);
        }

        wgtLearner.show();
        wgtLang.destroy();

    },

    onLeanerSelection: function(){
        console.log('InterfaceVideoPlayerScreen.onLearnerSelection');
        this.vurl = this.getUrl();
        var epicResults = App.data.epicResults;
        var wgtLearner = this.getWidget('learnerDialog');
        
        if (epicResults.learners.length >= 1) {
            console.log('InterfaceVideoPlayerScreen.onLearnerSelection learners.length: ' + epicResults.learners.length);
            this.allowRewind = true;
            this.startVideo();
            wgtLearner.destroy();
        }
    },

    showVideoComprehensionDialog: function(){
        var wgtVidComp = this.getWidget('videoComprehensionDialog');
        this.allowRewind = false;
        wgtVidComp.show();
        wgtVidComp.getWidget('buttons').focus('#yes');
        this.onKey = wgtVidComp.onKey;
    },
    onVideoComprehensionSelection: function(){
        console.log('InterfaceVideoPlayerScreen.onVideoComprehensionSelection');
        var self = this;
        self.sendResults()
            .done(function(){
                console.log('InterfaceVideoPlayerScreen.onVideoComprehensionSelection: SENT RESULTS');

                var wgtVidComp = self.getWidget('videoComprehensionDialog');
                wgtVidComp.destroy();

                if(App.data.epicResults.understanding == 'NL') {
                    if(self._isWelcomeVideo()) {
                        console.log('InterfaceVideoPlayerScreen.onVideoComprehensionSelection. Going home after welcome video');
                        App.router.go('home');
                    } else {
                        console.log('InterfaceVideoPlayerScreen.onVideoComprehensionSelection. Going back after interface video');
                        self.back();
                    }
                } else {
                    self.showFeedbackDialog();
                }
            });
        
    },

    showFeedbackDialog: function(){
        var self = this;
        console.log('InterfaceVideoPlayerScreen.showFeedbackDialog');
        var widget = this.getWidget('feedbackDialog');
        widget.show();
        widget.getWidget('buttons').focus('#close');
        self.onKey = widget.onKey;
        
    },
    onFeedbackSelection: function(){
        var self = this;
        console.log('InterfaceVideoPlayerScreen.onFeedbackSelection');
        if(self.isWelcomeVideo) { 
            App.router.go('home');
        } else {
            self.back();
        }
    },

    closePlayer: function(){
        var self = this;
        this.sendResults()
            .done(function(){
                // App.router.go('intro');
                self.back();
            });
    },

    getUrl: function () {
        var self = this;
        var vurl = '';

        $.each(self.video.renditions, function (i, rendition) {
            console.log(rendition.type);

            if (rendition.type == 'Original') { // this is the default url if no language matches
                if (App.config.originalUrl) {
                    vurl = rendition.metadatas.originalUrl || rendition.metadatas.url;
                } else {
                    vurl = rendition.metadatas.url || rendition.metadatas.originalUrl;
                }
            }
            var lang = (rendition.metadatas.language || 'english').toLowerCase();
            console.log(rendition.metadatas.originalUrl);
            if (lang == App.data.epicResults.renditionlang) {
                if (App.config.originalUrl) {
                    vurl = rendition.metadatas.originalUrl || rendition.metadatas.url;
                } else {
                    vurl = rendition.metadatas.url || rendition.metadatas.originalUrl;
                }
            }
        });

        return vurl;
    },

    sendResults: function () {
        var self = this;
        var defer = jQuery.Deferred();
        console.log('App.data.epicResults');
        console.log(App.data.epicResults);

        if (this.position >= 1) {
            upserver.api('/me/education', 'POST',
                {
                    "bookmarkId": self.bookmarkId,
                    "language": App.data.epicResults.renditionlang,
                    "learner": App.data.epicResults.learners.join(','),
                    "completion": App.data.epicResults.completion,
                    "understanding": App.data.epicResults.understanding
                }).done(function(){
                    console.log('InterfaceVideoPlayer.sendResults - Completion: ' + App.data.epicResults.completion);
                    console.log('InterfaceVideoPlayer.sendResults.bookmarkId: ' + self.bookmarkId);
                    console.log('InterfaceVideoPlayer.sendResults - Results Sent');
                    defer.resolve();
                });
        } 

        return defer.promise();
    },

    _isWelcomeVideo: function(){
        var rt = App.router.getPath();
        var isit = rt.indexOf('welcome-video');
        console.log('InterfaceVideoPlayer._isWelcomeVideo: ' + isit);
        return (isit > 0) ? true : false;
    }
});

export default InterfaceVideoPlayerScreen;