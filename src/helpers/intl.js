import i18next from 'i18next';

class Intl {
    
    static translate (str, lang) {
        var lang = lang || App.data.language;
        return i18next.t(str, {lng: lang});
    }
}

export default Intl;