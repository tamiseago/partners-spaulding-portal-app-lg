class SetCenter {

    static getCenterX(obj)
    {
        var width = obj.width() || 1280;
        var objWidth = obj.parent().width() || 1102;
        var x = (width / 2) - (objWidth / 2);
        console.log('Object Helper - Window Width:', width);
        console.log('Object Helper - Object Width:', objWidth);
        console.log('Object Helper - Object X:', x);

        return x;
    }

    static getCenterY(obj)
    {
        var height = obj.parent().height() || 720;
        var objHeight = obj.width() || 526;
        console.log('Object Helper - Window Height:', height);
        console.log('Object Helper - Object Height:', objHeight);
        console.log('Object Helper - Object Y:', y);

        var y = (height / 2) - (objHeight / 2); // + ($("body").scrollTop());
        return y;
    }

    static setCenter(obj)
    {
        var self = SetCenter;

        var x = self.getCenterX(obj);
        var y = self.getCenterY(obj);

        obj.css("top", y + $(window).scrollTop());        
        obj.css("left", x);
    }

    static getMaxHeight(rcObj)
    {
        var heights = rcObj.map(function ()
        {
            return $(this).height();
        }).get();
        return maxHeight = Math.max.apply(null, heights);
    }
}

export default SetCenter;