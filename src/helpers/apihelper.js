/**
 * Handy helper functions to get most common data from upserver
 * This class is a static class with all methods being static
 */
class ApiHelper {

    static getPreference(pref, scope) {
        scope = scope || 'PatientVisit';
        return upserver.api('/me/preferences/' + pref, 'GET', {
            scope: scope
        });
    }

    static setPreference(pref, val, scope) {
        scope = scope || 'PatientVisit';
        return upserver.api('/me/preferences', 'POST', {
            key: pref,
            value: val,
            scope: scope
        });
    }

    static calcDurationInSeconds(duration) {
        console.log("ApiHelper.calcDurationInSeconds duration: " + duration);

        if (!duration)
            return parseInt(0);

        var nums = duration.split(':');
        if( nums.length == 1) {
            return parseInt(duration);
        }
        console.log("ApiHelper.calcDurationInSeconds nums:" + JSON.stringify(nums));

        if(nums.length == 3) {
            var hh = parseInt(nums[0]) * 60;
            var mm = hh + parseInt(nums[1]);
            var ss = parseInt(nums[2]);
            console.log("ApiHelper.calcDurationInSeconds hh:" + hh);
        } else if(nums.length == 2) {
            var mm = parseInt(nums[0]);
            var ss = parseInt(nums[1]);
        }

        var sec = (mm * 60) + ss;

        console.log("ApiHelper.calcDurationInSeconds mm:" + mm);
        console.log("ApiHelper.calcDurationInSeconds ss:" + ss);
        console.log("ApiHelper.calcDurationInSeconds sec:" + sec);

        return sec;
    }



}

export default ApiHelper;
